<?php 
function obterModelo($modelo) {
	if (is_file( $modelo)) { 
		return file_get_contents($modelo); 
	} else{
		return FALSE;
	}
}
####################################################################

function colocarModelo($modelo, $vetor) {	
	foreach ($vetor as $a => $b) {
		$modelo = str_replace( '{'.$a.'}', $b, $modelo );
	}
	return $modelo; 
}
