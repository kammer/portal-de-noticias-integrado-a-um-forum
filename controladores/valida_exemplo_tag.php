<?php
function valida_exemplo_tag($exemplo){
	
	$tam_exemplo = strlen($exemplo);

	if (empty($exemplo)) {
		$erros['exemplo_vazio'] = "O campo EXEMPLO TAG está vazio!";
	}elseif ($tam_exemplo < 25) {
		$erros['tam_exemplo'] = "O campo EXEMPLO TAG tem que ter no minimo 25 caracteres!";
	}

	
	if ($tam_exemplo>=10000) {
		$erros['tam_exemplo'] = "O campo EXEMPLO TAG excedeu o limite de caracteres!";
	}
	if (isset($erros)) {
		return $erros;
	}else{
		return 1;
	}

}