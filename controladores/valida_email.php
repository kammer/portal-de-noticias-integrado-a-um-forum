<?php 

function valida_email($email){

	$conexao = obterConexao();

	$consulta = $conexao->query("SELECT email FROM usuario");

	$consulta = $consulta->fetchAll(PDO::FETCH_ASSOC);

	for ($i=0; $i < count($consulta); $i++) { 
		
		if ($email == $consulta[$i]['email']) {
		
			$erros['email_igual'] = "Este EMAIL já esta cadastrado!";
		
		}

	}

	if (empty($email)){
		
		$erros['email_vazio'] = "O campo EMAIL está vazio!";
		
	}

	$email = preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.com]$/', $email);

	if ($email == 0) {
		
		$erros['email_padrao'] = "O campo EMAIL não esta em um padrão válido!";

	}

	if (isset($erros)) {
	
		return $erros;

	}else{

		return 1;

	}
}