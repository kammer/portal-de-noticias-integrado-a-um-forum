<?php
	function valida_nome($nome){
		$nome = preg_replace('/[^a-zA-Z]/', '', (string)$nome);
		$tam_nome = strlen($nome);

		
		if (is_numeric($nome)) {
			$erros['nome_numero'] = "Não podem conter numeros no campo NOME!";
		}

		if (empty($nome)) {
			$erros['nome_vazio'] = "O campo NOME não pode estar vazio!";
		}

		elseif($tam_nome<=2) {
			$erros['nome_tamanho'] = "O tamanho do seu NOME tem que ser maior que 2!";
		}
		if(isset($erros)){
			return $erros;
		}else{
			return 1;
		}
	}