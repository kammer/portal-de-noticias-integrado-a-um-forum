<?php
	require_once '../base/conexao.php';

	require_once 'valida_titulo_tag.php';
	require_once 'valida_texto_tag.php';
	require_once 'valida_exemplo_tag.php';
	require_once 'valida_linguagem_tag.php';

	require_once 'cadastrador_tag.php';
	
	function trata_tag($titulo , $texto_tag , $exemplo ,  $ling){
		$titulo_validado  = valida_titulo_tag($titulo);
		$texto_validado   = valida_texto_tag($texto_tag);
		$exemplo_validado = valida_exemplo_tag($exemplo);
		$ling_validado    = valida_linguagem_tag($ling);
		

		if ($titulo_validado == 1 && $texto_validado == 1 && $exemplo_validado == 1 && $ling_validado == 1){
			$cadastro_tag = cad_tag($titulo , $texto_tag , $exemplo ,  $ling);
			
			if ($cadastro_tag == 1) {
				$msg = "Tag cadastrada com sucesso";
				$retorno = array('msg'=>$msg);
				return $retorno;
			}else{
				$msg = "Erro no cadastro";
				$retorno = array('msg' =>$msg);
			}
		}else{
			$retorno = array(
				'msg'=>$mensagens = [$titulo_validado,$texto_validado,$exemplo_validado,$ling_validado],
				'titulo'=>$titulo,
				'texto'=>$texto_tag,
				'exemplo'=>$exemplo,
				'ling'=>$ling);
			return $retorno;
		}

	}