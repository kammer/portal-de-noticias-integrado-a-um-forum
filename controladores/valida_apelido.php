<?php 

	function valida_apelido($apelido){

		$conexao = obterConexao();

		$consulta = $conexao->query("SELECT apelido FROM usuario");

		$consulta = $consulta->fetchAll(PDO::FETCH_ASSOC);

		for ($i=0; $i < count($consulta); $i++) { 

			if ($apelido == $consulta[$i]['apelido']) {

				$erros['apelido_igual'] = "Este APELIDO já esta cadastrado!";

			}

		}

		if (empty($apelido)) {
			
			$erros['apelido_vazio'] = "O campo APELIDO está vazio!";
			
		}

		$apelido = preg_match('/^[^0-9][a-zA-Z0-9]+$/', $apelido);

		if ($apelido == 0) {
			
			$erros['apelido_padrao'] = "O campo APELIDO não esta em um padrão válido!";

		}

		if (isset($erros)) {
			
			return $erros;

		}else{

			return 1; 
		
		}
	}