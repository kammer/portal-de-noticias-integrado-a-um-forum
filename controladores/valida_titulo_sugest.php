<?php 
function valida_titulo_sugest($titulo){
	if (empty($titulo)) {
		$erros['titulo_vazio'] = "O campo titulo esta vazio";
	}
	$tam_titulo = strlen($titulo);
	if ($tam_titulo >= 80) {
		$erros['tam_titulo'] = "O titulo excedeu o limite de caracteres";
	}

	if (isset($erros)) {
		return $erros;
	}else{
		return 1;
	}

}