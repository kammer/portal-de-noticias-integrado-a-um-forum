<?php
    session_start();    

    if ($_SESSION['situacao'] == 1) {
        
        unset(
            $_SESSION['usuarioId'],
            $_SESSION['usuarioNome'],
            $_SESSION['usuarioEmail'],
            $_SESSION['usuarioSenha'],
            $_SESSION['is_user_logged_in'],
            $_SESSION['situacao']
            );    

        session_destroy();
        session_start();    

        $_SESSION['banido'] = "Sua conta foi Bloqueada!";
        header("Location:../estatico/login.php");

    }else{

        unset(
            $_SESSION['usuarioId'],
            $_SESSION['usuarioNome'],
            $_SESSION['usuarioEmail'],
            $_SESSION['usuarioSenha'],
            $_SESSION['is_user_logged_in'],
            $_SESSION['situacao']
            );    

        session_destroy();
        session_start();       

    $_SESSION['logindeslogado'] = "Deslogado com sucesso";

    }

  header("Location:../estatico/login.php");