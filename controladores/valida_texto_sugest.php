<?php 
function valida_texto_sugest($texto){
	if (empty($texto)) {
		$erros['texto_vazio'] = "O campo texto esta vazio";
	}
	$tam_texto = strlen($texto);
	if ($tam_texto >= 1000) {
		$erros['tam_texto'] = "O texto excedeu o limite de caracteres";
	}

	if (isset($erros)) {
		return $erros;
	}else{
		return 1;
	}
}