<?php 
function valida_titulo_not($titulo){
	
	$tam_titulo = strlen($titulo);

	if (empty($titulo)) {
		$erros['titulo_vazio'] = "O campo TÍTULO DA NOTICIA esta vazio!";
	}elseif ($tam_titulo < 5) {
		$erros['tam_titulo'] = "O campo TÍTULO DA NOTÍCIA tem que ter no minimo 5 carcateres!";
	}
	if ($tam_titulo >= 240) {
		$erros['tam_titulo'] = "O campo TÍTULO DA NOTÍCIA excedeu o limite de caracteres!";
	}

	if (isset($erros)) {
		return $erros;
	}else{
		return 1;
	}

}