<?php
function valida_senhas($senha, $senha2){
	
	$tam_senha = strlen($senha);

	if (empty($senha2) || empty($senha)) {
	
		$erros['senha_vazio'] = "Os campos SENHA e REPITA A SENHA esta/estão vazio(os)!";
	
	}
	
	if ($senha != $senha2){
	
		$erros['senha_igual'] = "Os campos SENHA e REPITA A SENHA estão diferentes!";
	
	}

	if ($tam_senha < 8){

		$erros['senha_tamanho'] = "Sua SENHA deve ter no minimo 8 Caracteres!";
	
	}

	if (!preg_match("#[0-9]+#", $senha)) {
		
		$erros['senha_conter_numero'] = "Sua SENHA deve conter um numero!";

	}

	if (!preg_match("#[a-zA-Z]+#", $senha)) {
		
		$erros['senha_conter_letra'] = "Sua SENHA dever conter uma letra!";

	}

	if(isset($erros)){
	
		return $erros;
	
	}else{
	
		return 1;
	
	}

}