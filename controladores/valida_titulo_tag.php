<?php 
function valida_titulo_tag($titulo){
	
	$tam_titulo = strlen($titulo);

	if (empty($titulo)) {
		$erros['titulo_vazio'] = "O campo TÍTULO DA TAG esta vazio";
	}elseif ($tam_titulo < 5) {
		$erros['tam_titulo'] = "O campo TÍTULO DA TAG tem que ter no minimo 5 caracteres!";
	}
	if ($tam_titulo >= 80) {
		$erros['tam_titulo'] = "Foi excedido o limite de caracteres no campo TÍTULO DA TAG!";
	}


	if (isset($erros)) {
		return $erros;
	}else{
		return 1;
	}
}
