<?php 
function valida_texto_tag($texto){
	
	$tam_texto = strlen($texto);
	
	if (empty($texto)) {
		$erros['texto_vazio'] = "O campo TEXTO SOBRE A TAG está vazio!";
	}elseif ($tam_texto < 25) {
		$erros['tam_texto'] = "O campo TEXTO SOBRE A TAG tem que ter no minimo 25 caracteres!";
	}
	if ($tam_texto >= 2000){
		$erros['tam_texto'] = "Foi excedido o limite de caracteres no campo TEXTO SOBRE A TAG!";
	}

	if (isset($erros)) {
		return $erros;
	}else{
		return 1;
	}
}	