<?php 
function valida_texto_not($texto){
	
	$tam_texto = strlen($texto);

	if (empty($texto)) {
		$erros['texto_vazio'] = "O campo TEXTO DA NOTÍCIA esta vazio!";
	}elseif ($tam_texto < 25) {
		$erros['texto_vazio'] = "O campo TEXTO DA NOTÍCIA tem que ter no minimo 25 caracteres!";
	}
	if ($tam_texto >= 5000) {
		$erros['tam_texto'] = "O campO TEXTO DA NOTÍCIA excedeu o limite de caracteres";
	}

	if (isset($erros)) {
		return $erros;
	}else{
		return 1;
	}
}