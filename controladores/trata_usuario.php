
<?php 
require_once '../controladores/cadu_user.php';
require_once 'valida_nome.php';
require_once 'valida_apelido.php';
require_once 'valida_email.php';
require_once 'valida_senha.php';
require_once 'valida_profissao.php';

function trata_usuario($nome , $apelido , $email , $senha ,$senha2, $ocupacao){

	$nome_validado     = valida_nome($nome);
	$apelido_validado  = valida_apelido($apelido);
	$email_validado    = valida_email($email);
	$senha_validado    = valida_senhas($senha ,$senha2);
	$profissao_validado = valida_profissao($ocupacao);

	if ($nome_validado == 1 && $apelido_validado == 1 && $email_validado == 1 && $senha_validado == 1 && $profissao_validado == 1 ) {

		$cadastro_usuario = cad_user($nome , $apelido , $email , $senha ,$senha2, $ocupacao);

		if ($cadastro_usuario == 1) {

			$msg = "Usuário Cadastrado com Sucesso!";
			$retorno = array('msg'=>$msg);
			return $retorno;

		}else{

			$msg = "Erro no Cadastro!";
			$retorno = array('msg'=>$msg);
			return $retorno;

		}
		
	}else{

		$retorno = array(
			'msg'=>$mensagens = [$nome_validado,$apelido_validado,$email_validado,$senha_validado,$profissao_validado],
			'nome'=>$nome,
			'apelido'=>$apelido,
			'email'=>$email,
			'senha'=>$senha,
			'senha2'=>$senha2,
			'ocupacao'=>$ocupacao);

		return $retorno ;
		
	}
}
