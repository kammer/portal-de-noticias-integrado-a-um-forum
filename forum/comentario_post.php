<?php 
require_once 'controlador/seleciona_post_comentario.php';
include_once '../base/cabecalho.php';
require_once 'controlador/comentario.php';
require_once 'controlador/select_comentario.php';
require_once 'controlador/data_atual.php';
require_once 'controlador/resposta_comentario.php';
require_once 'controlador/seleciona_resposta.php';
require_once 'controlador/edita_comentario.php';
require_once 'controlador/edita_resposta.php';
include_once '../perfil/controlador/seleciona_imagem.php';

if (isset($_POST['enviarEdicaoResposta'])) {
	edita_resposta($_POST['texto_edicao'], $_GET['id'], $_POST['data_edicao'], $_POST['id_resposta']);

}
if (isset($_POST['enviarEdicaoComentario'])) {
	edita_comentario($_POST['texto_edicao'], $_GET['id'], $_POST['data_edicao'], $_POST['id_comentario']);

}
if (isset($_POST['enviarComentario'])) {
	$retorno = comentario($_POST['comentario'], $_GET['id'], $_SESSION['usuarioId'], $_POST['data_comentario']);

}
if (isset($_POST['enviarResposta'])) {
	$retorno = resposta_comentario($_POST['resposta'], $_POST['id_comentario'], $_SESSION['usuarioId'], $_POST['data_resposta'],  $_GET['id']);

}

$exibe_posts = select_post_comentario($_GET['id'] );

?>

<div class="ui main text container">

	<h2 style="word-wrap: break-word;" class="centralizar"><?php echo ($exibe_posts['titulo_post']); ?></h2>
	<p  class="text_direita"><?php echo "Autor: ".($exibe_posts['nome'])." (".($exibe_posts['profissao']).")" ;?></p>
	<p class="text_direita"><?php echo "Data de Publicação: ".($exibe_posts['data_publicacao']); ?></p>
	<p style="word-wrap: break-word;" class="text_justifica"><?php echo ($exibe_posts['texto_post']); ?></p>

	<hr>

	<div class="ui threaded comments divComentario">
		<h3 class="ui dividing header" id="comentarios">Comentarios</h3>
		<?php 

		$coments = seleciona_comentario($_GET['id']);

		if (!empty($coments)) {
			foreach ($coments as $exibe_comentarios) {
				$img = seleciona_img($exibe_comentarios['id_user']);

				?>
				<div class="comment">
					<a class="avatar">

						<?php if (!isset($img[0])): ?>

							<img src="../img/perfil.jpg" class="ui circular avatar image">

						<?php else: ?>

							<img src="<?php echo($img[0]['caminho_midia']) ?>" class="ui circular avatar image">

						<?php endif ?>
					</a>
					<div class="content">
						<a class="author"><?php echo $exibe_comentarios['apelido']; ?> (<?php echo $exibe_comentarios['profissao']; ?>)</a>
						<div class="metadata">
							<span class="date"><?php echo $exibe_comentarios['data_comentario']; ?></span>
						</div>
						<div class="text">
							<?php echo $exibe_comentarios['texto_comentario'] ?>
						</div>
						<div class="actions">
							<a class="reply responder">Responder</a>

							<?php 

							if (isset( $_SESSION['usuarioId'])) {
								if($exibe_comentarios['id_user'] == $_SESSION['usuarioId']){

									?>

									<a href="controlador/deletar_comentario.php?id=<?php echo$exibe_comentarios['id_coment']; ?>&id_post=<?php echo$_GET['id']; ?>"><i class="remove icon"></i></a>
									<a class="editar"><i class="write icon"></i></a>

									<?php 

								}
							}

							?>

							<form class="ui reply form formEditar" method="POST" style="display: none;">
								<div class="field">
									<textarea name="texto_edicao"><?php echo $exibe_comentarios['texto_comentario']; ?></textarea>
									<input type="hidden" value="<?php echo $exibe_comentarios['id_coment']; ?>" name="id_comentario">
									<input type="hidden" value="<?php $data = dataAtual(); echo$data; ?>"  name="data_edicao">
								</div>
								<button class="ui green inverted labeled submit icon button" name="enviarEdicaoComentario" type="submit">
									<i class="icon edit"></i> Confirmar Edição
								</button>
							</form>

							<form class="ui reply form formResponder" method="POST" style="display: none;">
								<div class="field">
									<textarea name="resposta"></textarea>
									<input type="hidden" value="<?php echo $exibe_comentarios['id_coment']; ?>" name="id_comentario">
									<input type="hidden" value="<?php $data = dataAtual(); echo$data; ?>"  name="data_resposta">
								</div>
								<button class="ui green inverted labeled submit icon button" name="enviarResposta" type="submit">
									<i class="icon edit"></i> Adicionar Resposta
								</button>
							</form>

						</div>
					</div>

					<?php
					$respostas = seleciona_resposta($exibe_comentarios['id_coment']);
					if (!empty($respostas)) {

						foreach ($respostas as $exibe_respostas) {
							$img_resp = seleciona_img($exibe_respostas['id_user']);
							if ($exibe_comentarios['id_coment'] == $exibe_respostas['id_comentario_resposta']) {

								?>

								<div class="comments" style="">
									<div class="comment">
										<a class="avatar">

											<?php if (!isset($img_resp[0])): ?>

												<img src="../img/perfil.jpg" class="ui circular avatar image">

											<?php else: ?>

												<img src="<?php echo($img_resp[0]['caminho_midia']) ?>" class="ui circular avatar image">

											<?php endif ?>
										</a>
										<div class="content">
											<a class="author"><?php echo $exibe_respostas['apelido']; ?></a>
											<div class="metadata">
												<span class="date"><?php echo $exibe_respostas['data_resposta']; ?></span>
											</div>
											<div class="text">
												<?php echo $exibe_respostas['texto_resposta']; ?>
											</div>
											<div class="actions">

												<?php 

												if (isset( $_SESSION['usuarioId'])) {
													if($exibe_respostas['id_user'] == $_SESSION['usuarioId']){

														?>

														<a href="controlador/deletar_resposta.php?id=<?php echo$exibe_respostas['id_resposta']; ?>&id_post=<?php echo$_GET['id']; ?>"><i class="remove icon"></i></a>
														<a class="editar"><i class="write icon"></i></a>

														<?php 

													}
												}

												?>

												<form class="ui reply form formEditar" method="POST" style="display: none;">
													<div class="field">
														<textarea name="texto_edicao"><?php echo $exibe_respostas['texto_resposta']; ?></textarea>
														<input type="hidden" value="<?php echo $exibe_respostas['id_resposta']; ?>" name="id_resposta">
														<input type="hidden" value="<?php $data = dataAtual(); echo$data; ?>"  name="data_edicao">
													</div>
													<button class="ui green inverted labeled submit icon button" name="enviarEdicaoResposta" type="submit">
														<i class="icon edit"></i> Confirmar Edição
													</button>
												</form>

											</div>
										</div>
									</div>
								</div>

								<?php 
							}
						} 
						?>

						<?php
						;}
					}
				}
				?>
			</div>
			<?php if(!is_user_logged_in()){ ?>
			<a href="../estatico/login.php" class="ui item">
				<button class="ui orange fluid button">Para criar um novo COMENTARIO, efetue LOGIN</button>
			</a>
			<?php } else{ ?>

			<form class="ui reply form formComentario" method="POST">
				<div class="field">
					<textarea name="comentario"></textarea>
					<input type="hidden" id="" name="data_comentario" value="<?php $data = dataAtual(); echo$data; ?>">
				</div>
				<button class="ui green inverted labeled submit icon button" name="enviarComentario" type="submit">
					<i class="icon edit"></i> Adicionar Comentario
				</button>
			</form>

			<?php } ?>


		</div>
	</div>
</div>
</div>
<?php include_once '../base/rodape.php'; ?>