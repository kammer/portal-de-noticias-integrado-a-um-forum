<?php 
include_once '../base/cabecalho.php'; 
include_once "controlador/seleciona_topico.php"; 
include_once "controlador/numero_posts.php";
require_once "controlador/pesquisa_post.php";
require_once '../perfil/controlador/seleciona_imagem.php';
require_once 'controlador/temp_limite.php';
if (isset($_POST['busca'])) {
  $busca_posts = busca_posts($_POST['busca']);

}
?>
<div class="ui main text container">
  <h1 class="ui dividing header">Forum - Programação IFC</h1>



  <?php 

  if (isset($busca_posts)) { ?>

  <?php if ($busca_posts == 0) { ?>

  <div class="ui red floating message">
    <div class="header">
      Houve um erro com sua Pesquisa:
    </div>
    <ul class="list">
      <li>Preencha o campo de PESQUISA!</li>
    </ul>
  </div>
  <a href="index.php">
    <div class="ui vertical labeled icon buttons">
      <button  class="ui inverted green button">
        <i class="arrow circle outline left icon"></i>
        Voltar
      </button>
    </div>
  </a>

  <?php }elseif ($busca_posts == 1) { ?>

  <div class="ui red floating message">
    <div class="header">
      Houve um erro com sua Pesquisa:
    </div>
    <ul class="list">
      <li>Nenhum post encontrado com esse TÍTULO!</li>
    </ul>
  </div>
  <a href="index.php">
    <div class="ui vertical labeled icon buttons">
      <button  class="ui inverted green button">
        <i class="arrow circle outline left icon"></i>
        Voltar
      </button>
    </div>
  </a>

  <?php } ?>

  <?php if (is_array($busca_posts)) { 

    ?>

    <div class="ui relaxed divided items">

      <?php
      foreach ($busca_posts as $exibe_posts) {
        $id = $exibe_posts['id_post'];
        $img = seleciona_img($exibe_posts['id_user']);
        $descricao = limitar_texto($exibe_posts['texto_post'], $limite = 95);         
        ?>  
        <div class="item">
          <div class="content">
            <a class="header" href="comentario_post.php?id=<?php echo$id;?>"><?php echo ($exibe_posts['titulo_post']); ?></a>
            <div class="meta">
              <a><?php echo ($exibe_posts['data_publicacao']); ?></a>
              <a>Tópico : <?php echo ($exibe_posts['nome_ling']); ?></a>
            </div>
            <div class="description">
              <?php echo $descricao; ?>
            </div>
            <div class="extra">

              <?php if (!isset($img[0])): ?>

                <img src="../img/perfil.jpg" class="ui circular avatar image"> <?php echo ($exibe_posts['nome']); 
                ?> (<?php echo ($exibe_posts['profissao']); ?>)
              <?php else: ?>

                <img src="<?php echo($img[0]['caminho_midia']) ?>" class="ui circular avatar image"> <?php echo ($exibe_posts['nome']); 
                ?> (<?php echo ($exibe_posts['profissao']); ?>)

              <?php endif ?>

              <?php
              if (isset( $_SESSION['usuarioId'])) {
                if($exibe_posts['id_user'] == $_SESSION['usuarioId']){

                  ?>
                  <a href="edita_post.php?id=<?php echo$id;?>&id_topico=<?php echo$_GET['id_topico'];?>">
                    <button class="ui icon right floated button">
                      <i class="setting icon"></i>
                    </button>
                  </a>
                  <a href="controlador/deleta_post.php?id=<?php echo$id;?>">
                    <i class="trash icon"></i>
                  </a>
                  <?php
                }
              }
              ?>
            </div>
          </div>
        </div>

        <?php } ?>
      </div>
      <?php
    }
  }else{
    ?>

    <div class="ui grid">
      <div class="eight wide column">
        <?php if(isset($_SESSION['tipo_user']) && $_SESSION['tipo_user'] == 2){ ?>
        <a href="cadastro_topico.php">
          <div class="ui vertical labeled icon buttons">
            <button  class="ui button">
              <i class="add circle icon"></i>
              Criar um novo Tópico
            </button>
          </div>
        </a>
        <?php } ?>
      </div>
      <div class="eight wide column">
        <form class="ui form" method="POST">
          <i class="search icon"></i>
          <label>Buscar por Título do Post:</label>
          <input value="" type="text" name="busca" placeholder="Buscar um POST">
        </form>
      </div>
    </div><hr>  
    <div class="ui segment">
      <h2 class="ui dividing header">Tópicos</h2>
      
      <?php 
      $topicos = seleciona_topicos();
      foreach ($topicos as $exibe_topicos) {
        $numero_posts = numero_posts($exibe_topicos['id_ling']);?>

        <div class="ui segment">
          <div class="ui items">
            <div class="item">
              <div class="content">
                <a href="index_forum.php?id_topico=<?php echo$exibe_topicos['id_ling'];?>&pag_atual=1" class="header"><?php echo$exibe_topicos['nome_ling']; ?> 
                </a>
                <div class="meta">
                  <span></span>
                </div>
                <div class="description">
                  <p><?php echo$exibe_topicos['texto_ling']; ?></p>
                </div>
                <div class="extra">
                  Criado Por: <?php echo$exibe_topicos['nome']; ?> (ADM)  
                  <div class="right floated"><?php echo $numero_posts; ?> Posts
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <?php } ?>
      </div>
      <?php } ?>
    </div>


    <?php include_once '../base/rodape.php'; ?>