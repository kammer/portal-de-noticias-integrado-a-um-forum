<?php 
require_once '../base/cabecalho.php';
require_once 'controlador/seleciona_post_especifico.php';
require_once 'controlador/edit_postagem.php';

$retorno = seleciona_post_especifico($_GET['id']);
$id_topico = $_GET['id_topico'];

if (isset($_POST['enviar'])) {
	edita_post($_POST['novo_titulo'], $_POST['novo_texto'], $_GET['id'], $id_topico);
}
?>

<section class="ui main text container">
	<h1 class="centralizar">Editar Post</h1>
	<form class="ui form" method="POST">
		<label>Novo Título</label>
		<input type="text" name="novo_titulo" value="<?php echo $retorno['titulo_post']; ?>">

		<label>Novo Texto</label>
		<input type="text" name="novo_texto" value="<?php echo $retorno['texto_post']; ?>">

		<input class="ui inverted right floated green button" type="submit" name="enviar">
	</form>
</section>
