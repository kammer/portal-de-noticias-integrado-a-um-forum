<?php require_once '../base/cabecalho.php'; ?>

<?php
require_once 'controlador/seleciona_topico.php';
require_once 'controlador/trata_post.php';
require_once '../base/conexao.php';
include_once 'controlador/data_atual.php';

$_POST['id_usuario'] = $_SESSION['usuarioId'];
$data = dataAtual();

if (isset($_POST['enviar'])) {
	$retorno_post = trata_post($_POST['titulo'], $_POST['conteudo'] , $_POST['id_usuario'], $data, $_POST['topico']);	

	if ($retorno_post == "Post cadastrado com sucesso") {
		$conexao = obterConexao();
		$consulta = $conexao->query("SELECT id_post FROM post, usuario WHERE id_user = ".$_SESSION['usuarioId']." ORDER BY id_post DESC");
		$consulta = $consulta->fetch(PDO::FETCH_ASSOC);
		header("Location:comentario_post.php?id=".$consulta['id_post']);
	}else{

		$mensagem = $retorno_post;
	}
}
?>
<div class="ui main text container">
	<?php if (!isset($mensagem)) {
		?>

		<div class="ui floating message">
			<div class="header">
				Regras:
			</div>
			<ul class="list">
				<li>O TITULO tem que ter no minimo 5 e no maximo 30 Caracteres.</li>
				<li>O TEXTO tem que ter no minimo 25 e no maximo 10000 caracteres.</li>
			</ul>
		</div>
		<?php }else{ ?>
		<div class="ui red floating message">
			<div class="header">
				Alguns erros foram encontrados ao fazer o cadastro de POST:
			</div>
			<ul class="list">

				<?php
				foreach ($mensagem as $msg) { ?>

				<?php if (!is_array($msg)){ ?>
				<li><?php echo($msg);?> </li> 
				<?php 	}} ?>

			</ul>
		</div>

		<?php } ?>

		<form class="ui form" method="POST">
			<h2 class="ui dividing header">Cadastro de Posts</h2>
			<div class="eight wide field">
				<label>Título POST</label>

				<input type="Text" name="titulo" value="<?php if(isset($mensagem['post']['titulo'])){echo$mensagem['post']['titulo']; unset($mensagem['post']['titulo']);} ?>">
			</div>
			<div class="fields">
				<div class="eight wide field">
					<label>Selecione um Tópico</label>
					<select class="ui fluid dropdown" name="topico">
						
						<?php if (!isset($mensagem['post']['topico'])) {?>
						<?php } ?>
						<?php $topicos = seleciona_topicos();

						if (!isset($mensagem['post']['topico']) || empty($mensagem['post']['topico'])){ ?>

						<option value="">Selecione uma Opção</option>

						<?php 

						foreach ($topicos as $option) {
							?>

							<option value="<?php echo$option['id_ling']; ?>"><?php echo $option['nome_ling']; ?></option>

							<?php }

						}else{ ?>

						<?php 

						$conexao = obterConexao();
						$consulta =  $conexao->query('SELECT nome_ling FROM ling_prog WHERE id_ling = '.$mensagem['post']['topico']);
						$retorno =   $consulta->fetchAll(PDO::FETCH_ASSOC);

						?>

						<option value="<?php echo$mensagem['post']['topico']; ?>"><?php echo $retorno[0]['nome_ling']; ?></option>

						<?php foreach ($topicos as $option) { ?>

						<?php if ($mensagem['post']['topico'] != $option['id_ling']): ?>

							<option value="<?php echo$option['id_ling']; ?>"><?php echo $option['nome_ling']; ?></option>

						<?php endif  ?>

						<?php } var_dump($retorno); ?>

						<?php } ?>


					</select>
				</div>
			</div>
			<div class="field">
				<label>Texto POST</label>
				<textarea name="conteudo"><?php if(isset($mensagem['post']['texto'])){echo$mensagem['post']['texto']; unset($mensagem['post']['texto']);} ?></textarea>
			</div>

			<input class="ui inverted right floated green button" type="submit" name="enviar">
		</form>
	</div>

	<?php include_once '../base/rodape.php'; 

