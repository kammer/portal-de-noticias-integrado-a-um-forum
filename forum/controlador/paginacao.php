<?php 
//require_once '../../base/conexao.php';
require_once 'numero_posts.php';
require_once 'seleciona_posts.php';

function paginacao ($id_ling , $pagina){
	$numero_posts = numero_posts($id_ling);
	$limite_paginacao = 5;

	$qtd_necessaria_pag = $numero_posts / $limite_paginacao;
	$qtd_necessaria_pag = ceil($qtd_necessaria_pag);

	if (empty($pagina)) {
		$pag_atual = 1;
	}else{
		$pag_atual = $pagina;
	}

	$inicio_select = $pag_atual - 1;
	$inicio_select = $inicio_select * $limite_paginacao;

	$consulta_posts = select_posts($id_ling, $inicio_select, $limite_paginacao);

	return $consulta_posts;

}