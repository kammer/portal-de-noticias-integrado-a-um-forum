<?php
require_once 'cad_post.php';
function trata_post($titulo, $texto , $id, $data, $id_ling){
	if (strlen($titulo) < 5) {
		$erros['titulo'] = "O campo TíTULO tem que ter no minimo 5 caracteres!"; 
	}elseif (strlen($titulo) > 30) {
		$erros['titulo'] = "O campo TÍTULO tem que ter no maximo 30 caracteres!";
	}
	if (strlen($texto) < 25) {

		$erros['texto'] = "O campo TEXTO tem que ter no minimo 25 caracteres!";

	}elseif (strlen($texto) > 10000) {
		
		$erros['texto'] = "o campo TEXTO tem que ter no maximo 10000 caracteres!";

	}
	if (empty($id_ling)) {
		$erros['ling'] = "Você deve selecionar um TÓPICO!";
	}



	if (isset($erros)) {
		$erros['post'] = array(
			'titulo'=>$titulo,
			'texto'=>$texto,
			'topico'=>$id_ling);
		return $erros;
		exit();
	}else{

		$retorno = cad_post($titulo, $texto , $id, $data, $id_ling);

		if ($retorno == "Post cadastrado com sucesso") {
			return $retorno;
		}else{
			return "Falha no cadastro";
		}
	}
}