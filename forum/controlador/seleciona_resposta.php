<?php
 require_once '../base/conexao.php';

function seleciona_resposta($id_comentario){
	$conexao = obterConexao();

	$consulta = $conexao->query("SELECT texto_resposta, apelido, id_comentario_resposta, data_resposta, id_resposta, id_user FROM usuario, resposta_comentario WHERE id_comentario_resposta = '$id_comentario' AND id_user = id_usuario_resposta ORDER BY id_resposta; ");
	$consulta = $consulta->fetchall(PDO::FETCH_ASSOC);

	return $consulta;
}