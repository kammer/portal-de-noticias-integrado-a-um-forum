 <?php

function busca_posts($busca){

$conexao = obterConexao();

if(!empty($busca)){
	$stmt = $conexao->prepare("SELECT id_post, titulo_post , texto_post , nome, id_user, data_publicacao, apelido, nome_ling, profissao from post, usuario, ling_prog where titulo_post like :busca AND usuario_id_user = id_user AND id_post_ling = id_ling");
	$stmt->bindValue(':busca', '%'.$busca.'%', PDO::PARAM_STR);
	$stmt->execute();
	$resultados = $stmt->rowCount();
	$pesquisa = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if ($resultados == 0) {
		return 1;

	}else{
		return $pesquisa;
	}  

}else{
	return 0;

}
}
