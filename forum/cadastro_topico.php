<?php require_once '../base/cabecalho.php'; ?>

<?php
require_once 'controlador/seleciona_topico.php';
require_once 'controlador/trata_post.php';
require_once '../base/conexao.php';
include_once 'controlador/data_atual.php';
include_once 'controlador/cad_topico.php';



if (isset($_POST['enviar'])) {
	$retorno = cad_topico($_POST['texto'], $_SESSION['usuarioId'], $_POST['titulo']);	

	if ($retorno = "Post cadastrado com sucesso") {

		header("Location:index.php");

	}else{
		$cor = "color: red;";
	}
}
?>
<div class="ui main text container">
	<form class="ui form" method="POST">
		<h2 class="ui dividing header">Cadastro de Topico</h2>
		<?php 
		if (isset($cor)) {
			?>
			<p style="<?php echo $cor;?>">
				<?php echo $retorno; ?>
			</p>			
			<?php
		}
		?>
		<div class="eight wide field">
			<label>Título Tópico (Linguagem de Programação)</label>
			<input type="Text" name="titulo">
		</div>
		<div class="field">
			<label>Texto POST</label>
			<textarea name="texto"></textarea>
		</div>

		<input class="ui inverted right floated green button" type="submit" name="enviar">
	</form>
</div>

<?php include_once '../base/rodape.php'; 