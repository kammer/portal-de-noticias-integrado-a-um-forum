<?php 
require_once 'controlador/seleciona_posts.php';
require_once 'controlador/paginacao.php';
require_once 'controlador/numero_posts.php';
include_once '../perfil/controlador/seleciona_imagem.php';
include_once 'controlador/temp_limite.php';
?>
<?php require_once '../base/cabecalho.php'; ?>
<div class="ui main text container">
	<h1 class="ui dividing header">Forum - Programação IFC</h1>
	<div class="ui grid">
		<div class="eight wide column">
			<?php if(!is_user_logged_in()){ ?>

			<a href="../estatico/login.php" class="ui item">
				<div class="ui vertical labeled icon buttons">
					<button  class="ui button">
						<i class="add circle icon"></i>
						Para criar um novo POST, efetue LOGIN
					</button>
				</div>
			</a>
			<?php } else{ ?>
			<a href="cadastro_post.php"> 
				<div class="ui vertical labeled icon buttons">
					<button  class="ui button">
						<i class="add circle icon"></i>
						Criar um novo post
					</button>
				</div>
			</a>
			<?php } ?>
		</div>
	</div>
	<hr>
	<div class= "ui container">
		<div class="ui relaxed divided items">
			<!-- ############################################### -->
			<?php if(isset($_POST['busca'])){ ?>

			<h2  class="centralizar">Posts Encontrados</h2>
			<?php
			require_once 'controlador/pesquisa_post.php';
			$busca=$_POST['busca'];
			$posts = busca_posts($busca);

			if (is_array($posts)) {
				var_dump($posts);
				foreach ($posts as $exibe_posts) {
					?>

					<h2 style="word-wrap: break-word;" class="centralizar"> <?php echo ($exibe_posts['titulo_post']); ?></h2>
					<p  class="text_direita"><?php echo ($exibe_posts['nome']); ?></p>
					<p class="text_direita"><?php echo ($exibe_posts['data_publicacao']); ?></p>

					<?php

					if (isset( $_SESSION['usuarioId'])) {
						if($exibe_posts['id_user'] == $_SESSION['usuarioId']){
							$id = $exibe_posts['id_post'];
							?>

							<a href="edita_post.php?id=<?php echo$id;?>">
								<button class="ui icon button">
									<i class="setting icon"></i>
								</button>
							</a>

							<?php
						}
					}
				}
				?>

				<p style="word-wrap: break-word;" class="text_justifica"><?php echo ($exibe_posts['texto_post']); ?></p>
				<hr>
				<?php 
			}

			?>

			<?php }





			else{ ?>

			<h2 class="centralizar">Posts mais recentes:</h2>
			<?php
			if (isset($_GET['id_topico'])) {
				$posts = paginacao($_GET['id_topico'], $_GET['pag_atual']);
			}


			foreach ($posts as $exibe_posts) {

				$id = $exibe_posts['id_post'];
				$img = seleciona_img($exibe_posts['id_user']);
				$descricao = limitar_texto($exibe_posts['texto_post'], $limite = 95);
				?>

				<div class="item">
					<div class="content">
						<a class="header" href="comentario_post.php?id=<?php echo$id;?>"><?php echo ($exibe_posts['titulo_post']); ?></a>
						<div class="meta">
							<a><?php echo ($exibe_posts['data_publicacao']); ?></a>
							<a>Tópico : <?php echo ($exibe_posts['nome_ling']); ?></a>
						</div>
						<div class="description">
							<?php echo $descricao; ?>
						</div>
						<div class="extra">

							<?php if (!isset($img[0])): ?>

								<img src="../img/perfil.jpg" class="ui circular avatar image"> <?php echo ($exibe_posts['nome']); 
								?> (<?php echo ($exibe_posts['profissao']); ?>)
							<?php else: ?>

								<img src="<?php echo($img[0]['caminho_midia']) ?>" class="ui circular avatar image"> <?php echo ($exibe_posts['nome']); 
								?> (<?php echo ($exibe_posts['profissao']); ?>)

							<?php endif ?>

							<?php
							if (isset( $_SESSION['usuarioId'])) {
								if($exibe_posts['id_user'] == $_SESSION['usuarioId']){

									?>
									<a href="edita_post.php?id=<?php echo$id;?>&id_topico=<?php echo$_GET['id_topico'];?>">
										<button class="ui icon right floated button">
											<i class="setting icon"></i>
										</button>
									</a>
									<a href="controlador/deleta_post.php?id=<?php echo$id;?>">
										<i class="trash icon"></i>
									</a>
									<?php
								}
							}
							?>
						</div>
					</div>
				</div>





				<?php 
			}
		}	
		?>

		<?php 	

		$anterior = $_GET['pag_atual'] - 1;
		$proximo = $_GET['pag_atual'] + 1;
		$id_topico = $_GET['id_topico'];
		if($_GET['pag_atual']>1) {
			echo "<a href='?id_topico=$id_topico&pag_atual=$anterior'><button class='ui button'>Anterior</button></a>";
		}
		$qtd_necessaria_pag = numero_posts($_GET['id_topico']) / 5;
		if ($_GET['pag_atual']<$qtd_necessaria_pag) {
			echo "<a href='?id_topico=$id_topico&pag_atual=$proximo'><button class='ui right floated button'>Proxima</button></a>";
		} ?>
	</div>
</div>
</div>
</div>
<?php require_once '../base/rodape.php'; ?>

