-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 24-Nov-2017 às 17:24
-- Versão do servidor: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prfc`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentarios`
--

CREATE TABLE `comentarios` (
  `id_coment` int(11) NOT NULL,
  `texto_comentario` varchar(1000) DEFAULT NULL,
  `usuario_id_user` int(11) DEFAULT NULL,
  `id_post` int(11) DEFAULT NULL,
  `data_comentario` varchar(45) DEFAULT NULL,
  `status_comentario` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ling_not`
--

CREATE TABLE `ling_not` (
  `id_ling` int(11) DEFAULT NULL,
  `id_not` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ling_prog`
--

CREATE TABLE `ling_prog` (
  `id_ling` int(11) NOT NULL,
  `nome_ling` varchar(80) DEFAULT NULL,
  `texto_ling` varchar(10000) DEFAULT NULL,
  `id_ling_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `ling_prog`
--

INSERT INTO `ling_prog` (`id_ling`, `nome_ling`, `texto_ling`, `id_ling_usuario`) VALUES
(1, 'PHP', 'O PHP (um acrônimo recursivo para PHP: Hypertext Preprocessor) é uma linguagem de script open source de uso geral, muito utilizada, e especialmente adequada para o desenvolvimento web e que pode ser embutida dentro do HTML.', 27),
(2, 'HTML', 'HTML (abreviação para a expressão inglesa HyperText Markup Language, que significa Linguagem de Marcação de Hipertexto) é uma linguagem de marcação utilizada na construção de páginas na Web. ', 27),
(3, 'CSS', 'Cascading Style Sheets (CSS) é um simples mecanismo para adicionar estilo (cores, fontes, espaçamento, etc.) a um documento web', 27),
(5, 'JavaScript', 'JavaScript é uma linguagem de programação baseada em scripts e padronizada pela ECMA International (associação especializada na padronização de sistemas de informação). Foi criada por Brendan Eich (Netscape) e surgiu em 1995 como linguagem de script client-side de páginas web.', 27),
(6, 'Mysql', 'O MySQL é um sistema de gerenciamento de banco de dados (SGBD), que utiliza a linguagem SQL (Linguagem de Consulta Estruturada, do inglês Structured Query Language) como interface', 27),
(7, 'Python', 'Python é uma linguagem de programação criada por Guido van Rossum em 1991. Os objetivos do projeto da linguagem eram: produtividade e legibilidade. Em outras palavras, Python é uma linguagem que foi criada para produzir código bom e fácil de manter de maneira rápida.', 27);

-- --------------------------------------------------------

--
-- Estrutura da tabela `midia`
--

CREATE TABLE `midia` (
  `id_midia` int(11) NOT NULL,
  `caminho_midia` varchar(300) NOT NULL,
  `id_usuario_midia` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `midia`
--

INSERT INTO `midia` (`id_midia`, `caminho_midia`, `id_usuario_midia`) VALUES
(5, '../img/perfil/d75e9ebb40dfe7869a8a0a6966d0b563.jpg', 27);

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

CREATE TABLE `noticias` (
  `titulo_not` varchar(240) DEFAULT NULL,
  `data` varchar(11) DEFAULT NULL,
  `texto_not` varchar(5000) DEFAULT NULL,
  `id_not` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `noticias`
--

INSERT INTO `noticias` (`titulo_not`, `data`, `texto_not`, `id_not`) VALUES
('PHP recebe a Versão 15.0', '27-10-2017', 'Agora que o PHP 7.0 está finalmente lançado, cabe aos usuários PHP decidir se e quando eles vão adotá-lo em seus projetos.\r\n\r\nLeia este artigo para aprender sobre a história do PHP 7, os principais novos recursos desta versão, os resultados de uma pesquisa realizada nas últimas semanas para avaliar as intenções dos usuários de PHP em adotar o PHP 7, e o que esperar para o futuro do PHP.\r\n\r\nA história do PHP 7\r\n\r\nPHP 7 é o primeiro grande lançamento PHP desde a versão 5.0.0, que foi lançada em 2004, há mais de 11 anos.\r\n\r\nNão é como se os core developers do PHP estivessem sem trabalhar todo esse tempo. PHP 6 tinha um ambicioso plano para trazer suporte transparente para strings Unicode ao PHP. No entanto, o seu desenvolvimento estagnou e PHP 6 foi cancelado em 2010.\r\n\r\nApós esse período, PHP teve vários lançamentos com alguns novos recursos importantes, como namespaces no PHP 5.4, mas nada realmente grande que justificasse o lançamento de uma versão maior.\r\n\r\nAinda em 2010, o Facebook anunciou o compilador PHP HipHop. Ele foi um compilador que iria converter PHP em código C++, que então seria compilado em código de máquina nativo com um compilador C++. Apesar de o conceito ser ótimo e de ele ter trazido algumas melhorias de desempenho para PHP, não era muito prático, porque levaria muito tempo para compilar scripts PHP para código de máquina nativo.\r\n\r\nDepois disso, o Facebook mudou para uma abordagem diferente. Eles criaram a HHVM (HipHop Virtual Machine), que compila o PHP em código de máquina nativo usando um engine JIT (Just In Time). Isso levaria muito menos tempo e ainda alcançou melhorias significativas de desempenho.\r\n\r\nMas, aparentemente, o Facebook não estava feliz com as features do PHP e, em 2014, lançou Hack, uma linguagem derivada do PHP que trouxe muitas características que faltavam no PHP, como mais rigor na checagem de tipos em todos os tipos de dados e suporte nativo de programação assíncrona.\r\n\r\nNão muito tempo depois, Dmitry Stogov, da Zend, anunciou um projeto paralelo que estava um pouco em segredo para o desenvolvimento de PHP, que foi chamado de PHPNG.\r\n\r\nOs principais novos recursos do PHP 7\r\n\r\nInicialmente, a ideia de PHPNG foi investigar a introdução de um engine JIT que iria trabalhar com a versão do Zend Engine baseada em PHP. No entanto, com mais investigação, ele percebeu que outras melhorias poderiam ser realizadas no código PHP para fazê-lo rodar muito mais rápido.\r\n\r\nPHPNG se tornou a base da próxima versão PHP, que foi chamada PHP 7 para evitar confusão com o PHP 6, que teve seus planos originais cancelados.\r\n\r\nMuitos outros recursos foram adicionados ao PHP 7, incluindo tipo estrito para valores escalares, classes anônimas, classes aninhadas, e a possibilidade de compilar a engine PHP e otimizar para aplicações específicas, como WordPress, usando Performance Guided Optimization (PGO).\r\n\r\nPHP 7.0 foi originalmente concebido para ser lançado em outubro, mas como havia ainda alguns bugs, o lançamento foi adiado até dezembro.\r\n\r\nResultado de pesquisa: adoção do PHP 7\r\n\r\nA versão 7 do PHP é definitivamente um dos eventos mais emocionantes no mundo do PHP nos últimos tempos devido a muitos dos recursos previstos.\r\n\r\nMuitos desenvolvedores estão ansiosos para começar a usar o PHP 7, mas nem todos eles vão utilizá-lo imediatamente. Portanto, uma pesquisa foi criada para avaliar as intenções dos desenvolvedores PHP em adotar PHP 7.\r\n\r\nBasicamente, eram três perguntas:\r\n\r\nVocê usará o PHP 7 em produção?\r\nVocê usará PHP 7 em seu ambiente de desenvolvimento?\r\nQual é a versão mais recente do PHP que você está usando em produção?\r\n526 desenvolvedores responderam à pesquisa, por isso os resultados devem refletir a realidade das opiniões de muitos desenvolvedores PHP.\r\n\r\n1. Você usará o PHP 7 em produção?\r\n\r\n01\r\n\r\nSim, eu já estou usando, desde a primeira versão estável do 7.0.0	21	4%\r\nSim, eu quero começar a usar quando a versão 7.0.0 for oficialmente liberada	104	19.8%\r\nSim, mas vou esperar algumas semanas ou meses após a versão 7.0.0 ser liberada	196	37.3%\r\nDepende dos clientes para os quais eu trabalho	44	8.4%\r\nNão, não usarei tão cedo. Eu preciso migrar um monte de código e isso vai me levar um longo tempo	58	11%\r\nNão, não agora, eu só pretendo usá-lo para futuros novos projetos	64	12.2%\r\nNão, somente se a minha empresa de hospedagem me obrigar a usá-lo e não fornecer uma versão mais antiga	20	3.8%\r\nOutros	19	3.6%\r\n2. Você usará PHP 7 em seu ambiente de desenvolvimento?\r\n\r\n02\r\n\r\nSim, eu já tenho usado continuamente por um tempo	41	7.8%\r\nSim, eu usei algumas vezes para testar suas features	60	11.4%\r\nSim, mas quero começar a usar somente depois que a versão oficial 7.0.0 for liberada	218	41.4%\r\nSim, mas vou esperar algumas semanas ou meses até que eu tenha mais tempo para verificar a nova versão	112	21.3%\r\nDepende dos clientes para os quais eu trabalho	11	2.1%\r\nNão, não agora, eu só pretendo usá-lo para futuros novos projetos	62	11.8%\r\nNão, somente se a minha empresa de hospedagem me o', 9),
('WTF/MIN TODOS JUNTOS', '27-10-2017', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam aliquam sem ut tortor sagittis, vitae feugiat justo lobortis. Vivamus eu semper sem, vitae tincidunt magna. Sed turpis nulla, bibendum vitae ullamcorper sed, aliquet et purus. Quisque elementum venenatis ipsum vitae sollicitudin. Vivamus consequat, lorem eu sodales elementum, nulla arcu consequat erat, quis pellentesque dui odio id nibh. Quisque ut erat felis. Suspendisse faucibus placerat arcu, id iaculis quam interdum at. Cras sit amet ipsum velit. Proin rhoncus massa in justo convallis hendrerit. Morbi mattis arcu eget odio pellentesque consectetur. Aenean accumsan dictum semper. Pellentesque a porta tortor. In non ornare enim, in tincidunt dolor. Sed a mauris quis ligula dictum vestibulum et in sapien. Donec diam velit, tempus id gravida eu, sagittis eget risus. Etiam at molestie leo, ut pharetra purus.\r\n\r\nQuisque fermentum, arcu at sodales consectetur, ligula nibh commodo lorem, eget feugiat nibh nulla vitae leo. Quisque pellentesque venenatis lorem facilisis dictum. Morbi ac metus ultrices, maximus tellus vel, efficitur risus. Maecenas et vestibulum nisl. Sed tristique nulla id imperdiet lacinia. Donec porttitor ante egestas felis sagittis vulputate. Maecenas aliquet nibh in lacus rutrum commodo.\r\n\r\nMauris enim lorem, porta quis est gravida, feugiat rhoncus nunc. In hac habitasse platea dictumst. Donec gravida nulla nec magna aliquam congue. Etiam ultrices orci id tellus tincidunt, at feugiat ex sagittis. Morbi quam leo, facilisis vel elementum sit amet, posuere eu nisl. In ac purus elit. Etiam ac nisi diam. Donec risus justo, elementum eget lacus non, eleifend aliquet justo. Curabitur quis luctus velit. Donec fermentum lacus vehicula neque viverra rutrum. Quisque in nisl pulvinar libero pellentesque porta id et ante. Pellentesque ut posuere quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis, magna in hendrerit interdum, lorem elit gravida quam, et egestas turpis elit at metus. Integer ipsum neque, vehicula nec mattis nec, rhoncus ut purus.\r\n\r\nNunc suscipit turpis ac mi dignissim luctus. Nullam scelerisque varius malesuada. Fusce tincidunt sed est eu fermentum. Nunc eleifend velit non elit ullamcorper iaculis. Mauris risus purus, sollicitudin eu odio vitae, fringilla luctus dolor. Aliquam sagittis libero fringilla, posuere risus ut, euismod enim. Nullam hendrerit, nulla id scelerisque convallis, mi massa malesuada magna, accumsan hendrerit elit ex quis lorem. Cras dictum leo vel sapien rhoncus pharetra. Sed facilisis erat ut efficitur sodales. Ut luctus lorem sem. Integer at metus vitae ipsum consectetur ultrices non vitae lectus. Cras pellentesque nisi enim, non commodo ante suscipit quis. In ultricies nunc sit amet sagittis dignissim.\r\n\r\nPraesent nec urna sem. Phasellus ullamcorper volutpat nulla in efficitur. Praesent finibus nisi lobortis, pulvinar ante quis, condimentum ligula. Nunc ultrices elit ac maximus vehicula. Fusce blandit suscipit scelerisque. Etiam vel tincidunt ligula. Nullam quis auctor est. Cras ut felis leo. Sed vulputate mauris ex, et mollis massa mattis ut. Morbi lectus dui, posuere nec metus et, semper finibus diam. Phasellus sed lectus blandit erat viverra rutrum sed nec tellus. Donec sed magna eu metus interdum euismod. Morbi aliquet magna ut mattis congue. Duis tincidunt, massa vitae porttitor rutrum, dolor arcu venenatis lacus, ut pellentesque felis ligula nec tortor. Aliquam nec rutrum ipsum. Aenean non nisl ornare augue tempus egestas.', 10),
('Aliquam nec rutrum ipsum.', '27-10-2017', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam aliquam sem ut tortor sagittis, vitae feugiat justo lobortis. Vivamus eu semper sem, vitae tincidunt magna. Sed turpis nulla, bibendum vitae ullamcorper sed, aliquet et purus. Quisque elementum venenatis ipsum vitae sollicitudin. Vivamus consequat, lorem eu sodales elementum, nulla arcu consequat erat, quis pellentesque dui odio id nibh. Quisque ut erat felis. Suspendisse faucibus placerat arcu, id iaculis quam interdum at. Cras sit amet ipsum velit. Proin rhoncus massa in justo convallis hendrerit. Morbi mattis arcu eget odio pellentesque consectetur. Aenean accumsan dictum semper. Pellentesque a porta tortor. In non ornare enim, in tincidunt dolor. Sed a mauris quis ligula dictum vestibulum et in sapien. Donec diam velit, tempus id gravida eu, sagittis eget risus. Etiam at molestie leo, ut pharetra purus.\r\n\r\nQuisque fermentum, arcu at sodales consectetur, ligula nibh commodo lorem, eget feugiat nibh nulla vitae leo. Quisque pellentesque venenatis lorem facilisis dictum. Morbi ac metus ultrices, maximus tellus vel, efficitur risus. Maecenas et vestibulum nisl. Sed tristique nulla id imperdiet lacinia. Donec porttitor ante egestas felis sagittis vulputate. Maecenas aliquet nibh in lacus rutrum commodo.\r\n\r\nMauris enim lorem, porta quis est gravida, feugiat rhoncus nunc. In hac habitasse platea dictumst. Donec gravida nulla nec magna aliquam congue. Etiam ultrices orci id tellus tincidunt, at feugiat ex sagittis. Morbi quam leo, facilisis vel elementum sit amet, posuere eu nisl. In ac purus elit. Etiam ac nisi diam. Donec risus justo, elementum eget lacus non, eleifend aliquet justo. Curabitur quis luctus velit. Donec fermentum lacus vehicula neque viverra rutrum. Quisque in nisl pulvinar libero pellentesque porta id et ante. Pellentesque ut posuere quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis, magna in hendrerit interdum, lorem elit gravida quam, et egestas turpis elit at metus. Integer ipsum neque, vehicula nec mattis nec, rhoncus ut purus.\r\n\r\nNunc suscipit turpis ac mi dignissim luctus. Nullam scelerisque varius malesuada. Fusce tincidunt sed est eu fermentum. Nunc eleifend velit non elit ullamcorper iaculis. Mauris risus purus, sollicitudin eu odio vitae, fringilla luctus dolor. Aliquam sagittis libero fringilla, posuere risus ut, euismod enim. Nullam hendrerit, nulla id scelerisque convallis, mi massa malesuada magna, accumsan hendrerit elit ex quis lorem. Cras dictum leo vel sapien rhoncus pharetra. Sed facilisis erat ut efficitur sodales. Ut luctus lorem sem. Integer at metus vitae ipsum consectetur ultrices non vitae lectus. Cras pellentesque nisi enim, non commodo ante suscipit quis. In ultricies nunc sit amet sagittis dignissim.\r\n\r\nPraesent nec urna sem. Phasellus ullamcorper volutpat nulla in efficitur. Praesent finibus nisi lobortis, pulvinar ante quis, condimentum ligula. Nunc ultrices elit ac maximus vehicula. Fusce blandit suscipit scelerisque. Etiam vel tincidunt ligula. Nullam quis auctor est. Cras ut felis leo. Sed vulputate mauris ex, et mollis massa mattis ut. Morbi lectus dui, posuere nec metus et, semper finibus diam. Phasellus sed lectus blandit erat viverra rutrum sed nec tellus. Donec sed magna eu metus interdum euismod. Morbi aliquet magna ut mattis congue. Duis tincidunt, massa vitae porttitor rutrum, dolor arcu venenatis lacus, ut pellentesque felis ligula nec tortor. Aliquam nec rutrum ipsum. Aenean non nisl ornare augue tempus egestas.', 11);

-- --------------------------------------------------------

--
-- Estrutura da tabela `notificacao`
--

CREATE TABLE `notificacao` (
  `id_notificacao` int(11) NOT NULL,
  `id_usuario_notificador` int(11) NOT NULL,
  `id_usuario_notificado` int(11) NOT NULL,
  `id_post_resposta` int(11) NOT NULL,
  `tipo_id_tipo_notificacao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `post`
--

CREATE TABLE `post` (
  `id_post` int(11) NOT NULL,
  `titulo_post` varchar(80) DEFAULT NULL,
  `texto_post` varchar(10000) DEFAULT NULL,
  `data_publicacao` varchar(45) NOT NULL,
  `usuario_id_user` int(11) DEFAULT NULL,
  `id_post_ling` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `resposta_comentario`
--

CREATE TABLE `resposta_comentario` (
  `id_resposta` int(11) NOT NULL,
  `texto_resposta` varchar(45) DEFAULT NULL,
  `id_comentario_resposta` int(11) NOT NULL,
  `id_usuario_resposta` int(11) NOT NULL,
  `data_resposta` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sugestoes`
--

CREATE TABLE `sugestoes` (
  `titulo_sug` varchar(80) DEFAULT NULL,
  `id_sug` int(11) NOT NULL,
  `texto_sug` varchar(1000) DEFAULT NULL,
  `usuario_id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tag`
--

CREATE TABLE `tag` (
  `id_tag` int(11) NOT NULL,
  `texto_tag` varchar(2000) DEFAULT NULL,
  `titulo_tag` varchar(80) DEFAULT NULL,
  `exemplo` mediumtext,
  `id_ling` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tag`
--

INSERT INTO `tag` (`id_tag`, `texto_tag`, `titulo_tag`, `exemplo`, `id_ling`) VALUES
(10, 'O paragraph tag é um tipo de elemento, usado para iniciar um novo parágrafo, os navegadores adicionam automaticamente um espaço em branco (uma margem) antes de depois do parágrafo. \r\n', '<p> </p>', '<p> Meu primeiro parágrafo </p>', 2),
(11, 'O breaking tag é um elemento, usada para “quebrar” uma linha do conteúdo, ou seja, pular uma linha do conteúdo de forma que ela fique em branco. Também podemos usar <br> dentro de um parágrafo, caso você não queira iniciar um novo.', '<br> </br> ', '<p> Meu parágrafo </p> <br> <p> Novo <br> parágrafo </p>', 2),
(12, 'Às horizontal roles tag em inglês, ou regras horizontais, são elementos usados para “quebra” de tema em uma página HTML. E normalmente são exibidas como linhas horizontais.\r\n', '<hr> </hr>', '<h1>Isso é um cabeçalho1</h1> <p>Isso é um parágrafo</p> <hr> <h2>Isso é um cabeçalho2</h2> <p>Isso é um parágrafo.</p> <hr>', 2),
(13, 'A tag <pre> é um elemento usado para definir um texto pré-formatado. Um texto dentro desse elemento é exibido em uma fonte de largura fixa, preserva espaços e quebras de linhas.\r\n', '<pre> </pre>', '<pre>     Fonte de largura fixa.     Preserva espaços.    Preserva quebras de linhas. </pre>', 2),
(14, 'O elemento <html> define todo o documento. Ele tem uma tag de início <html> e uma tag de fim </html>.\r\n', '<html> </html>', '<html> <body>   <h1>Meu titulo</h1> <p>Meu parágrafo</p>   </body> </html>', 2),
(15, 'O elemento <body> define todo conteúdo de do documento (corpo do documento). Ele tem uma tag de início <body> e uma tag de fim </body>.\r\n', '<body> </body>', '<body>   <h1>Meu titulo.</h1> <p>Meu paragrafo.</p>   </body>', 2),
(16, 'O elemento <head> é um contêiner para metadados. Os metadados HTML são dados sobre o documento HTML, os metadados não são exibidos. O elemento <head> é colocado entre a tag <html> e a tag <body>.\r\n', '<head> </head> ', '<body>   <h1>Meu titulo.</h1> <p>Meu parágrafo.</p>   </body>', 2),
(17, 'O idioma do documento pode ser declarado na tag <html>, o idioma é declarado com o atributo lang.\r\nDeclarar um idioma é importante para aplicações de acessibilidade e motores de busca.\r\nAs duas primeiras letras especificam o idioma (pt). Se houver um dialeto, use mais duas letras (br).\r\n', 'Atributo de linguagem.', '<!DOCTYPE html> <html lang=\"pt-br\"> <body> …   </body> </html>', 2),
(18, 'Os links são definidos na tag <a>, o endereço de link é especificado no atributo href.\r\n', 'Atributo href.', '<a href=\'https://google.com\'>Google </a>', 2),
(20, 'O título tooltip é um atributo title adicionado ao elemento <p>. O valor do atributo title será exibido como uma dica flutuante quando o mouse for passado sobre o parágrafo.	', 'Título tooltip.', '<p title=  “Eu sou uma dica flutuante”> Paragrafo </p>', 2),
(22, 'As Headings tags (h1,h2,h3…) são um tipo de elemento, usado para destacar títulos e subtítulos de uma página html. H1 é uma abreviação do inglês para Header, ou Cabeçalho. Conceitualmente o h1 possui um destaque maior, uma fonte maior, e é geralmente o texto mais visível da página.\r\nOs títulos são definidos com as tags vão de <h1> a <h6>. Essas tags tem uma ordem, onde o <h1> é o mais importante e o <h6> é o menos importante.\r\n', '<h1> </h1>', '<h1> 1-Cabeçalho  </h1> <h2> 2-Cabeçalho  </h2> <h3> 3-Cabeçalho  </h3> <h4> 4-Cabeçalho  </h4> <h5>5- Cabeçalho </h5> <h6>6- Cabeçalho  </h6>', 2),
(23, 'ela é um paragrafo', '<p>', '<p> oioi </p>', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_notificacao`
--

CREATE TABLE `tipo_notificacao` (
  `id_tip_notificacao` int(11) NOT NULL,
  `descricao_tipo_notificacao` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipo_notificacao`
--

INSERT INTO `tipo_notificacao` (`id_tip_notificacao`, `descricao_tipo_notificacao`) VALUES
(1, 'post'),
(2, 'comentario');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tip_user`
--

CREATE TABLE `tip_user` (
  `id_tipo` int(1) NOT NULL,
  `desc_tipo` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tip_user`
--

INSERT INTO `tip_user` (`id_tipo`, `desc_tipo`) VALUES
(2, 'Administrador'),
(3, 'Usuario');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_user` int(11) NOT NULL,
  `profissao` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `senha` varchar(40) DEFAULT NULL,
  `nome` varchar(30) DEFAULT NULL,
  `apelido` varchar(30) DEFAULT NULL,
  `id_tipo` int(1) DEFAULT NULL,
  `situacao` tinyint(1) NOT NULL,
  `token` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_user`, `profissao`, `email`, `senha`, `nome`, `apelido`, `id_tipo`, `situacao`, `token`) VALUES
(27, 'Profissional da Área', 'josekammer1@gmail.com', 'f34a755c1e2c111ea8059d967c5d5242', 'Raphael', 'Kammer', 2, 0, 'e6b0d7ee289d11887e3429badee8883651690050');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id_coment`),
  ADD KEY `id_user` (`usuario_id_user`),
  ADD KEY `id_post` (`id_post`);

--
-- Indexes for table `ling_not`
--
ALTER TABLE `ling_not`
  ADD KEY `id_ling` (`id_ling`),
  ADD KEY `id_not` (`id_not`);

--
-- Indexes for table `ling_prog`
--
ALTER TABLE `ling_prog`
  ADD PRIMARY KEY (`id_ling`),
  ADD KEY `id_user` (`id_ling_usuario`);

--
-- Indexes for table `midia`
--
ALTER TABLE `midia`
  ADD PRIMARY KEY (`id_midia`),
  ADD KEY `fk_id_usuario_midia` (`id_usuario_midia`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id_not`);

--
-- Indexes for table `notificacao`
--
ALTER TABLE `notificacao`
  ADD PRIMARY KEY (`id_notificacao`),
  ADD KEY `notificacao_ibfk_1` (`id_usuario_notificado`),
  ADD KEY `notificacao_ibfk_2` (`id_usuario_notificador`),
  ADD KEY `notificacao_ibfk_3` (`tipo_id_tipo_notificacao`),
  ADD KEY `teste_idx` (`id_post_resposta`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id_post`),
  ADD KEY `id_user` (`usuario_id_user`),
  ADD KEY `post_ibfk_2` (`id_post_ling`);

--
-- Indexes for table `resposta_comentario`
--
ALTER TABLE `resposta_comentario`
  ADD PRIMARY KEY (`id_resposta`),
  ADD KEY `fk_resposta_comentario_comentarios_idx` (`id_comentario_resposta`),
  ADD KEY `fk_resposta_comentario_usuario1_idx` (`id_usuario_resposta`);

--
-- Indexes for table `sugestoes`
--
ALTER TABLE `sugestoes`
  ADD PRIMARY KEY (`id_sug`),
  ADD KEY `id_user` (`usuario_id_user`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id_tag`),
  ADD KEY `id_ling` (`id_ling`);

--
-- Indexes for table `tipo_notificacao`
--
ALTER TABLE `tipo_notificacao`
  ADD PRIMARY KEY (`id_tip_notificacao`);

--
-- Indexes for table `tip_user`
--
ALTER TABLE `tip_user`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_tipo` (`id_tipo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id_coment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ling_prog`
--
ALTER TABLE `ling_prog`
  MODIFY `id_ling` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `midia`
--
ALTER TABLE `midia`
  MODIFY `id_midia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id_not` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `notificacao`
--
ALTER TABLE `notificacao`
  MODIFY `id_notificacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `resposta_comentario`
--
ALTER TABLE `resposta_comentario`
  MODIFY `id_resposta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sugestoes`
--
ALTER TABLE `sugestoes`
  MODIFY `id_sug` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id_tag` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tipo_notificacao`
--
ALTER TABLE `tipo_notificacao`
  MODIFY `id_tip_notificacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tip_user`
--
ALTER TABLE `tip_user`
  MODIFY `id_tipo` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `comentarios_ibfk_1` FOREIGN KEY (`usuario_id_user`) REFERENCES `usuario` (`id_user`),
  ADD CONSTRAINT `comentarios_ibfk_2` FOREIGN KEY (`id_post`) REFERENCES `post` (`id_post`);

--
-- Limitadores para a tabela `ling_not`
--
ALTER TABLE `ling_not`
  ADD CONSTRAINT `ling_not_ibfk_1` FOREIGN KEY (`id_ling`) REFERENCES `ling_prog` (`id_ling`),
  ADD CONSTRAINT `ling_not_ibfk_2` FOREIGN KEY (`id_not`) REFERENCES `noticias` (`id_not`);

--
-- Limitadores para a tabela `ling_prog`
--
ALTER TABLE `ling_prog`
  ADD CONSTRAINT `ling_prog_ibfk_1` FOREIGN KEY (`id_ling_usuario`) REFERENCES `usuario` (`id_user`);

--
-- Limitadores para a tabela `midia`
--
ALTER TABLE `midia`
  ADD CONSTRAINT `fk_id_usuario_midia` FOREIGN KEY (`id_usuario_midia`) REFERENCES `usuario` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `notificacao`
--
ALTER TABLE `notificacao`
  ADD CONSTRAINT `notificacao_ibfk_1` FOREIGN KEY (`id_usuario_notificado`) REFERENCES `usuario` (`id_user`),
  ADD CONSTRAINT `notificacao_ibfk_2` FOREIGN KEY (`id_usuario_notificador`) REFERENCES `usuario` (`id_user`),
  ADD CONSTRAINT `notificacao_ibfk_3` FOREIGN KEY (`tipo_id_tipo_notificacao`) REFERENCES `tipo_notificacao` (`id_tip_notificacao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`usuario_id_user`) REFERENCES `usuario` (`id_user`),
  ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`id_post_ling`) REFERENCES `ling_prog` (`id_ling`);

--
-- Limitadores para a tabela `resposta_comentario`
--
ALTER TABLE `resposta_comentario`
  ADD CONSTRAINT `fk_resposta_comentario_comentarios` FOREIGN KEY (`id_comentario_resposta`) REFERENCES `comentarios` (`id_coment`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_resposta_comentario_usuario1` FOREIGN KEY (`id_usuario_resposta`) REFERENCES `usuario` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `sugestoes`
--
ALTER TABLE `sugestoes`
  ADD CONSTRAINT `sugestoes_ibfk_1` FOREIGN KEY (`usuario_id_user`) REFERENCES `usuario` (`id_user`);

--
-- Limitadores para a tabela `tag`
--
ALTER TABLE `tag`
  ADD CONSTRAINT `tag_ibfk_1` FOREIGN KEY (`id_ling`) REFERENCES `ling_prog` (`id_ling`);

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_tipo`) REFERENCES `tip_user` (`id_tipo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
