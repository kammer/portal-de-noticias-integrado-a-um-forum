<?php include_once '../base/cabecalho.php'; ?>
<div class="ui main text container">
	<div class="ui middle aligned center aligned grid">
		<div class="column">
			<div class="ui stacked segment">

				<form class="ui form" method="POST" action="../controladores/val_login.php">
					<h2 class="ui dividing header">Login</h2>

					<div class="field">
						<div class="ui left icon input">
							<i class="user icon"></i>
							<input type="text" name="Email" placeholder="Insira seu Email">
						</div>				</div>
						<div class="field">
							<div class="ui left icon input">
								<i class="lock icon"></i>
								<input type="password" name="Senha" placeholder="****">
							</div>
						</div>
						<input class="positive ui button" value="Entrar" type="submit">
					</form>
				</div>

				<?php if(isset($_SESSION['loginErro'])){ ?>
				<div class="ui red message">
					<?php 
					echo $_SESSION['loginErro'];
					unset($_SESSION['loginErro']);
					?>
				</div>
				<?php }?>

				<?php if(isset($_SESSION['logindeslogado'])){ ?>
				<div class="ui green message">
					<?php 
					echo $_SESSION['logindeslogado'];
					unset($_SESSION['logindeslogado']);
					?>
				</div>
				<?php }  ?>

				<?php if(isset($_SESSION['banido'])){ ?>
				<div class="ui red message">
					<?php 
					echo $_SESSION['banido'];
					unset($_SESSION['banido']);
					?>
				</div>
				<?php }  ?>

				<?php if(isset($_SESSION['cadastrado_sucesso'])){ ?>
				<div class="ui green message">
					<?php 
					echo $_SESSION['cadastrado_sucesso'];
					unset($_SESSION['cadastrado_sucesso']);
					?>
				</div>
				<?php }  ?>

				<?php if(isset($_SESSION['senha_redefinida'])){ ?>
				<div class="ui green message">
					<?php 
					echo $_SESSION['senha_redefinida'];
					unset($_SESSION['senha_redefinida']);
					?>
				</div>
				<?php }  ?>

				<div class="ui message">
					Novo por aqui? <a href="cad_usuario.php">Cadastre-se</a><br>
					Esqueceu sua senha? <a href="recupera_senha.php">Recuperar</a>
				</div>
			</div>
		</div>
	</div>
	<?php include_once '../base/rodape.php'; ?>

