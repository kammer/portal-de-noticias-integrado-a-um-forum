<?php 
require_once '../controladores/select_linguagem.php';
$html = linguagem(2);

?>

<?php require_once '../base/cabecalho.php'; ?>
<div class="ui main text container">
	<?php 
	foreach ($html as $exibe_html) {
		?>	
		<h2 class="centralizar"><?php echo htmlspecialchars($exibe_html['titulo_tag']);?></h2>
		<section class="text_justifica">
			<p><?php echo htmlspecialchars($exibe_html['texto_tag']);?> </p>
			<p>Exemplo:</p>
			<p class="text_italic"> <?php echo htmlspecialchars($exibe_html['exemplo']) ;?> </p>
			<p>Como fica:  </p>
			<p>
				<?php echo $exibe_html['exemplo'];?> </p>
				<hr>
			</section>
			<?php 
		}

		?>
	</div>
	<?php require_once '../base/rodape.php'; ?>
