<?php

require_once '../controladores/select_linguagem.php';
$php = linguagem(1);

?>
<?php
require_once '../base/cabecalho.php'; ?>
<div class="ui main text container">

	<?php

	foreach ($php as $exibe_php) {

		?>	
		<h2 class="centralizar"><?php echo htmlspecialchars($exibe_php['titulo_tag']);?></h2>
		<section class="text_justifica">
			<p><?php echo htmlspecialchars($exibe_php['texto_tag']);?> </p>
			<p>Exemplo:</p>
			<p class="text_italic"> <?php echo htmlspecialchars($exibe_php['exemplo']) ;?> </p>
			<p>Como fica:  </p>
			<p>
				<?php echo $exibe_php['resultado'];?> </p>
				<hr>
			</section>
			<?php 
		}

		?>
	</div>

	<?php require_once '../base/rodape.php'; ?>