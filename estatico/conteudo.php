<?php

$linha = ['titulo1'=>'Tema','texto1'=>'Com a evolução da internet e o surgimento de chats e fóruns, as pessoas que utilizam a rede passam a utilizar novas maneiras de trocar mensagens e opiniões no ambiente virtual. É neste momento que começa a aparecer base para as primeiras redes sociais. Hoje é possível fazer cursos on-line, preencher formulários, conversar por chats ou expressar sua opinião em sites.
Segundo Magalhães, nos últimos 10 anos, a internet permitiu uma mudança na vida
das pessoas, tanto socialmente como	cientificamente. A quantidade de informação e
tecnologia transforma as pessoas comuns em formadores de opinião. A maior
parte destas informações está disponível em fóruns e redes sociais. Hoje também os
fóruns ensinam os mais variados assuntos para os seus usuários, sendo a maioria
deles de forma gratuita mas, na maioria dos casos, na língua inglesa.
Levando esse fato em consideração, será feito um site integrado a um fórum para ensinar, disponibilizar e ter essas informações em um só lugar. É muito útil hoje em dia, onde a agilidade para a obtenção de informações é almejada. Tendo isso em mente, o site será construído em etapas, ou seja, será construído em duas partes, primeira com conteúdo padrão (o básico da programação das linguagens mais comuns, softwares recomendados,dicas, um sistema de cores, etc.) e a segunda parte o fórum onde poderá ser postado uma dúvida e/ou código e alguém poderá auxiliar na construção de tal código ou a sanar a dúvida.
Este fórum será criado para minimizar a carência de sites/fórum de programação em português, formar um canal onde estudantes, professores e profissionais da área possam se comunicar, e também proporcionar um conteúdo para formar novos profissionais, adicionar novos conteúdos, resolver erros, desenvolver novas ideias proporcionando o crescimento de uma comunidade que não é grande aqui no Brasil. 
Para agilizar mais ainda o processo de pesquisa, sera adicionado mais temas relacionados a Informática em geral, ou seja, não terá somente a parte de programação, mas também será criada uma outras abas para outras áreas da informática, como, Hardware, Redes, Banco de Dados, segurança de rede, etc assim possibilitando uma gama maior de assuntos a discutir no fórum. 
',
'titulo2'=>'Problema','texto2'=>'Devido a grande procura de consultas de códigos/tags na internet, uma pesquisa confiável e integra e difícil de se obter, pois não há muitos sites confiáveis e que estejam “traduzidos” para nossa língua, dificultando assim qualquer pesquisa que tenha o objetivo de aprendizado, entendimento ou consulta. Como existem os mais variados tipos linguagens de programação, assim como elementos, muitas vezes o programador não se lembra de um elemento, porém surge uma duvida, site brasileiro ou estrangeiro. Mesmo com sites brasileiros, vários profissionais indicam os estrangeiros, que são mais “confiáveis”,  o grande problema de se consultar uma fonte em outra língua é a tradução do navegador, quando não se sabe o básico de tal língua, que traduz textos de maneira errada, e muitas vezes também traduz elementos que são em inglês. Isto faz com  que se abra dois caminhos para o problema, ou o programador aprende o inglês que está presente nas maioria dos sites ou ele procura em um livro na sua língua, porém a grande maioria dos programadores, trabalham com base em datas de entrega, não tem tempo para nenhum dos dois, por isso a rapidez na consulta é algo inestimável. Levando isso em consideração, a criação de um site que englobe as necessidades de consultas, tanto na parte estrangeira, como na parte brasileira para que auxilie tanto programadores a consultar outro profissional, quanto o próprio site ou auxilie estudantes em busca de conhecimento. 
',
'titulo3'=>'Justificativa','texto3'=>'Tendo em vista o problema, será feito um site para sanar a carência de web
sites em português e com um conteúdo de fácil entendimento e qualidade.  Já que o programador precisa de
uma fonte rápida, com qualidade e com exemplos, para lembrar de funções, e tags de cada língua de
programação.    No instituto, as aulas de programação, banco de dados entre outras matérias as vezes
demandam de pesquisa, que muitas vezes não são entendidas pois estão em outra língua, caso não tenha um
professor para auxiliar, o fórum integrado a um site seria um aglomerado de informações, assim facilitando
a pesquisa e agilizando-a.O projeto irá trazer um site em nossa língua, sem traduções diretas, onde terá um
fórum, que será usado para tirar dúvidas e proporcionar conversas entre profissionais da área e pessoas que
estão pensando em ingressar nesta carreira mas estão com dúvidas.',

'titulo4'=>'Onde posso conseguí-lo?','texto4'=>'Existem muitas variações disponíveis de passagens de Lorem Ipsum, 
mas a maioria sofreu algum tipo de alteração, seja por inserção de 
passagens com humor, ou palavras aleatórias que não parecem nem um 
pouco convincentes. Se você pretende usar uma passagem de Lorem Ipsum, 
precisa ter certeza de que não há algo embaraçoso escrito escondido 
no meio do texto. Todos os geradores de Lorem Ipsum na internet tendem 
a repetir pedaços predefinidos conforme necessário, fazendo deste o 
primeiro gerador de Lorem Ipsum autêntico da internet. Ele usa um 
dicionário com mais de 200 palavras em Latim combinado com um punhado
de modelos de estrutura de frases para gerar um Lorem Ipsum com 
aparência razoável, livre de repetições, inserções de humor, 
palavras não características, etc.'];
$template1 = obterModelo('../estatico/padrao_conteudo.php');
$templateFinal = colocarModelo( $template1, $linha );
echo $templateFinal;

