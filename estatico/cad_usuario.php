<?php 

require_once '../base/cabecalho.php'; 

require_once '../controladores/trata_usuario.php'; 

if (isset($_POST['Enviar'])){

    $retorno_cad = trata_usuario($_POST['nome'], $_POST['apelido'] , $_POST['email'] , $_POST['senha'] , $_POST['senha2'] , $_POST['profissao']);

    if ($retorno_cad['msg'] == "Usuário Cadastrado com Sucesso!") {

        $cor = "color: green;";

    }else{

        $cor = "color: red;";
        $nome = $retorno_cad['nome'];
        $apelido = $retorno_cad['apelido'];
        $email = $retorno_cad['email'];
        $senha = $retorno_cad['senha'];
        $senha2 = $retorno_cad['senha2'];
        $ocupacao = $retorno_cad['ocupacao'];
    }
}
?>
<div class="ui container">
    <div class="ui main text container">

        <?php if (!isset($retorno_cad['msg'])) {
            ?>

            <div class="ui floating message">
                <div class="header">
                    Regras:
                </div>
                <ul class="list">
                    <li>Nome: 3 ou mais caracteres. (Somente permitidas letras)</li>
                    <li>Senha: 8 ou mais caracteres. (Tendo no mínimo uma letra e um número)</li>
                </ul>
            </div>

            <?php }elseif (is_array($retorno_cad['msg'])) { ?>

            <div class="ui red floating message">
                <div class="header">
                    Alguns erros foram encontrados no seu CADASTRO:
                </div>
                <ul class="list">
                    
                    <?php

                    $mensagem = $retorno_cad['msg'];

                    foreach ($mensagem as $msg_array) {
                        if ($msg_array != 1) {
                            foreach ($msg_array as $msg) {
                                ?> <li><?php echo($msg); ?></li>
                                <?php            
                            }
                        }
                    }
                    ?>         </ul>
                </div>
                <?php 


            }else { 
                $_SESSION['cadastrado_sucesso'] = "Você foi cadastrado com Sucesso!";
                header("Location:login.php");}?>
                <div class="ui grid"> 
                    <div class="column">
                        <form class="ui form" method="POST">
                            <h2 class="ui dividing header">Cadastro</h2>
                            <div class="fields">	
                                <div class="eight wide field">
                                    <label>Nome</label>
                                    <input type="text" name="nome" placeholder="Insira seu nome" value="<?php if(isset($nome)){ echo $nome; unset($nome);}?>" data-inverted="" data-tooltip="Só permitidas letras e espaços!" data-position="bottom left">
                                </div>
                                <div class="eight wide field">
                                    <label>Apelido</label>
                                    <input type="text" name="apelido" placeholder="Insira seu Apelido" value="<?php if(isset($apelido)){  echo $apelido; unset($apelido);}?>">
                                </div>
                            </div>

                            <div class="fields">
                                <div class="eight wide field">
                                    <label>Email</label>
                                    <input type="email" name="email" placeholder="exemplo@mail.com" value="<?php if(isset($email)){ echo $email; unset($email);}?>">
                                </div>
                                <div class="eight wide field">
                                    <label>Ocupação</label>
                                    <select class="ui fluid dropdown" name="profissao">

                                        <?php if (!isset($ocupacao)): ?>

                                            <option value="">Selecione uma Opção</option>
                                            <option value="Profissional da Área">Profissional da Área</option>
                                            <option value="Professor">Professor</option>
                                            <option value="Aluno">Aluno</option>

                                        <?php elseif($ocupacao == "Profissional da Área"): ?>

                                            <option value="Profissional da Área">Profissional da Área</option>
                                            <option value="Professor">Professor</option>
                                            <option value="Aluno">Aluno</option>

                                        <?php elseif($ocupacao == "Professor"): ?>

                                            <option value="Professor">Professor</option>
                                            <option value="Profissional da Área">Profissional da Área</option>
                                            <option value="Aluno">Aluno</option>

                                        <?php elseif($ocupacao == "Aluno"): ?>

                                            <option value="Aluno">Aluno</option>
                                            <option value="Profissional da Área">Profissional da Área</option>
                                            <option value="Professor">Professor</option>

                                        <?php endif ?>
                                    </select>
                                </div> 
                            </div>

                            <div class="fields">
                                <div class="eight wide field">
                                    <label>Senha</label>
                                    <input type="password" name="senha" placeholder="********" value="<?php if(isset($senha)){ echo $senha; unset($senha); }?>">
                                </div>
                                <div class="eight wide field">
                                    <label>Repita a Senha</label>
                                    <input type="password" name="senha2" placeholder="********" value="<?php if(isset($senha2)){ echo $senha2; unset($senha2); }?>">
                                </div>
                            </div>
                            <div class="field">
                                <input class="ui inverted right floated green button" type="submit" name="Enviar" value="Enviar">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require_once '../base/rodape.php'; ?>