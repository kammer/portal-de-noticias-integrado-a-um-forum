<?php
require_once '../controladores/select_noticias.php';
require_once '../forum/controlador/temp_limite.php';

$noticias = select_noticias();
?>
<div class="ui segment transparent">
	<h1 class="ui center aligned dividing header">Noticias</h1>
	<div class="ui three column doubling stackable grid container">



		<?php 
		foreach ($noticias as $exibe_noticias) { ?>

		<div class="column">
			<div class="ui segment">
				<div class="ui relaxed divided items">
					<div class="item">
						<div class="content">
							<a class="header"><?php echo limitar_texto($exibe_noticias['titulo_not'], 35); ?></a>
							<div class="meta">
								<a>Data de publicação: <?php echo($exibe_noticias['data']); ?></a>
							</div>
							<div class="description">
								<?php echo limitar_texto($exibe_noticias['texto_not'], 500); ?>
							</div>
							<div class="extra">
								<?php if (is_user_logged_in()) { ?>
								<?php if ($_SESSION['tipo_user'] == 2) {?>
								<a href="../controladores/delete_noticia.php?id=<?php echo$exibe_noticias['id_not'];?>">
									<i class="trash icon"></i>
								</a>
								<?php 
							}
						} ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php }?>

</div>
</div>
