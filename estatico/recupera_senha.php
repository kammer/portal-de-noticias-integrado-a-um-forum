<?php 
include_once '../base/cabecalho.php';
require_once '../email/rec_senha.php';

if (isset($_POST['Enviar'])) {
	$rec_senha = recuperador_senha($_POST['Email']); 
}

?>
<div class="ui main text container">
	<div class="ui middle aligned center aligned grid">
		<div class="column">
			<div class="ui stacked segment">

				<form class="ui form" method="POST">
					<h2 class="ui dividing header">Recupere sua senha</h2>

					<div class="field">
						<div class="ui left icon input">
							<i class="mail icon"></i>
							<input type="text" name="Email" placeholder="Insira seu Email">
						</div>				
					</div>
						<input class="ui green inverted button" name="Enviar" type="submit">
					</form>
				</div>

			<?php
if (isset($rec_senha)) {
			 if ($rec_senha == 0) { ?>

	<div class="ui red floating message">
		<div class="header">
			Deu algo Errado:
		</div>
		<ul class="list">
			<li>Este email nao está cadastrado no sistema!</li>         
		</ul>
	</div>

	<?php }else{ ?>

	<div class="ui green floating message">
		<div class="header">
			Sucesso:
		</div>
		<ul class="list">
			<li>Verifique Sua Caixa de Entrada, Inclusive Sua Caixa de SPAM!.</li>         
		</ul>
	</div>	

	<?php } }?>
			</div>
		</div>
	</div>


	<?php include_once '../base/rodape.php'; ?>