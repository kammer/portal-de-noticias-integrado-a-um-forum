<?php

require_once '../controladores/select_linguagem.php';
$css = linguagem(3);

require_once '../base/cabecalho.php';
?>

<div class="ui main text container">
	<?php

	foreach ($css as $exibe_css) {
		?>	
		<h2 class="centralizar"><?php echo  htmlspecialchars($exibe_css['titulo_tag']);?></h2>
		<section class="text_justifica">
			<p><?php echo htmlspecialchars($exibe_css['texto_tag']);?> </p>
			<p>Exemplo:</p>
			<p class="text_italic"> <?php echo htmlspecialchars($exibe_css['exemplo']) ;?> </p>
			<p>Como fica:  </p>
			<p>
				<?php echo $exibe_css['resultado'];?> </p>
				<hr>
			</section>
			<?php 
		}

		?>
	</div>

	<?php require_once '../base/rodape.php'; ?>