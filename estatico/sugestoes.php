<?php
require_once '../base/cabecalho.php';
require_once '../controladores/trata_sugestao.php';
require_once '../controladores/cadastro_sugestao.php';

if (isset($_POST['enviar'])) {
    $retorno_sugestao = trata_sugestao($_SESSION['usuarioId'] , $_POST['texto_suget'], $_POST['titulo_suget']);
    if ($retorno_sugestao == "Usuário Cadastrado com Sucesso!") {
# code...
    }else{
        $titulo = $retorno_sugestao['titulo'];
        $texto = $retorno_sugestao['texto'];
    }
}


?>

<?php if(!is_user_logged_in()){ ?>
<div class="ui main text container">

    <div class="ui form">
        <h2 class="ui dividing header">Nos de sua Sugestão!</h2>
        <a href="../estatico/login.php" class="ui item">
            <button class="ui orange fluid button">Para criar uma SUGESTÃO, efetue LOGIN</button>
        </a>
    </div>
</div>
<?php } else{ ?>



<div class="ui main text container">
    <?php if (!isset($retorno_sugestao['msg'])) {
        ?>


        <?php }elseif (is_array($retorno_sugestao['msg'])) { ?>

        <div class="ui red floating message">
            <div class="header">
                Alguns erros foram encontrados no seu CADASTRO:
            </div>
            <ul class="list">
                <?php

                $mensagem = $retorno_sugestao['msg'];

                foreach ($mensagem as $msg_array) {
                    if ($msg_array != 1) {
                        foreach ($msg_array as $msg) {
                            ?> <li><?php echo($msg); ?></li>
                            <?php            
                        }
                    }
                }
                ?>         </ul>
            </div>
            <?php 


        }
        ?>
        <form class="ui form" method="POST">

            <h2 class="ui dividing header">Nos de sua Sugestão!</h2>
            <div class="eight wide field">
                <label>Título Sugestão</label>
                <input type="Text" name="titulo_suget" value="<?php if(isset($titulo)){
                    echo $titulo;
                    unset($titulo);
                } ?>">
            </div>
            <div class="field">
                <label>Sua Sugestão:</label>
                <textarea  name="texto_suget" rows="3"><?php if (isset($texto)) {
                    echo $texto;
                } ?></textarea>
            </div>

            <input class="ui inverted right floated green button" type="submit" name="enviar">
        </form>
    </div>
    <?php } ?>


    <?php require_once '../base/rodape.php'; ?>