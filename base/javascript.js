$( document ).ready(function() {

// Função para Fechar mensagens descartáveis
$('.message .close').on('click', function() {
  $(this).closest('.message').transition('fade');
});

$('.responder').on('click', function(){
  $(this).siblings('.formResponder').toggle();
});

$('.editar').on('click', function(){
  $(this).siblings('.formEditar').toggle();
});

$('.image')
.dimmer({
  on: 'hover'
});

$('.accordion')
.accordion({
  selector: {
    trigger: '.title .icon'
  }
});

$('.context.example .ui.sidebar')
.sidebar({
  context: $('.context.example .bottom.segment')
});

// JS Do Menu

$('.masthead')
.visibility({
  once: false,
  onBottomPassed: function() {
    $('.sec.menu').transition('fade in');
  },
  onBottomPassedReverse: function() {
    $('.sec.menu').transition('fade out');
  }
});

$('.ui.sidebar')
.sidebar('attach events', '.toc.item')
;

// Fim Menu
});