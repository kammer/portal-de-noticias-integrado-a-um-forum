<?php 
ini_set('display_errors',0);
ini_set('display_startup_erros',0);
error_reporting(E_ALL);
session_start();

include_once "../controladores/controle_usuario.php";
include_once "../controladores/controle_conteudo.php";
include_once "conexao.php"; 
include_once "../controladores/verifica_usuario.php";
include_once "../controladores/verifica_notificacao.php";
?>


<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <title>Programação IFC</title>

  <?php include_once "estatico.php";  ?>

  <script type="text/javascript">
    $( document ).ready(function() {

      function readImage() {
        if (this.files && this.files[0]) {
          var file = new FileReader();
          file.onload = function(e) {
            document.getElementById("preview").src = e.target.result;
          };       
          file.readAsDataURL(this.files[0]);
        }
      }

      document.getElementById("mudar-foto").addEventListener("change", readImage, false);

      $('.message .close')
      .on('click', function() {
        $(this)
        .closest('.message')
        .transition('fade')
        ;
      })
      ;

    });
  </script>
  
</head>
<body class="pushable">
  <article class="many pusher">
    <?php include_once 'menu.php'; ?>