<div class="ui vertical inverted sidebar menu">
            <a class="item" href="../estatico/html.php">HTML</a>
            <a class="item" href="../estatico/css.php">CSS</a>
            <a class="item" href="../estatico/php.php">PHP</a>
 <?php 
  if (isset($_SESSION['tipo_user'])) {
    if ($_SESSION['tipo_user'] == 2) { ?>

    <a href="../adm/visu_sugestoes.php" class="item responsivo">ADM</a>
    <?php } 
  }
  ?>
</div>

<div class="pusher">
  <div class="ui inverted vertical masthead center aligned segment">

    <div class="ui container">
      <div class="ui large secondary inverted pointing menu">
        <a class="toc item">
          <i class="sidebar icon"></i>
        </a>
        <a class="active item">Página Principal</a>
        
        <div class="ui simple dropdown item responsivo">Linguagens <i class="dropdown icon"></i>
          <div class="menu">

            <a class="item" href="../estatico/html.php">HTML</a>
            <a class="item" href="../estatico/css.php">CSS</a>
            <a class="item" href="../estatico/php.php">PHP</a>

          </div>

    </div>
        
    <?php 
    if (isset($_SESSION['tipo_user'])) {
      if ($_SESSION['tipo_user'] == 2) { ?>

      <a href="../adm/visu_sugestoes.php" class="item">ADM</a>
      <?php } 
    }
    ?>
        <div class="right item">
          <?php if(!is_user_logged_in()){ ?>
      <a href="../estatico/login.php" class="ui inverted button">Logar</a>
      <?php } else{ ?>

      <div class="ui simple dropdown">
        <i class="large icons">
          <i class="alarm outline icon"></i>
          <?php
            $notificacao = verifica_notificacao($_SESSION['usuarioId']);

           if (isset($notificacao)): ?>
          
          <i class="top right corner inverted green comments icon"></i>

          <?php endif ?>
        </i>
        <?= get_user_name(); ?>


        <i class="dropdown icon"></i>
        <div class="menu">
          <a class="item" href="../perfil/index_perfil.php">
          Perfil</a>
          <div class="divider"></div>
          <a href="../perfil/notificacoes.php" class="item">
            Notificações
            <?php if (isset($notificacao)): ?>
            
            <div class="ui label"><?php echo $notificacao; ?></div>

            <?php endif ?>
          </a>

          <a class="item" href="../controladores/sair.php">Sair</a>
        </div>
      </div>
      <?php } ?>
          <?php if (!is_user_logged_in()) { ?>
          <a href="../estatico/cad_usuario.php" class="ui inverted button">Registre-se</a>
          <?php } ?>
        </div>
      </div>
    </div>

    <div class="ui text container">
      <h1 class="ui inverted header">
        < Programação IFC />
      </h1>
      <h2>Voce tem alguma duvida?</h2>
      <a href="../forum" class="ui huge primary button">Pergunte no FORUM!<i class="right arrow icon"></i></a>
    </div>

  </div>
