<div class="ui large inverted fixed principal menu">
  <a class="toc item">
    <i class="sidebar icon"></i>
  </a>
  <a href="../index.php" class="item responsivo first">Programação IFC</a>
  <div class="ui simple dropdown item responsivo">Linguagens <i class="dropdown icon"></i>
    <div class="menu">

      <a class="item" href="../estatico/html.php">HTML</a>
      <a class="item" href="../estatico/css.php">CSS</a>
      <a class="item" href="../estatico/php.php">PHP</a>

    </div>

  </div>

  <?php 
  if (isset($_SESSION['tipo_user'])) {
    if ($_SESSION['tipo_user'] == 2) { ?>

    <a href="../adm/visu_sugestoes.php" class="item responsivo">ADM</a>
    <?php } 
  }
  ?>

  <a href="../forum" class="item responsivo">Fórum</a>
  <div class="right item">
    <?php if(!is_user_logged_in()){ ?>
    <a href="../estatico/login.php" class="ui inverted button">Logar</a>
    <?php } else{ ?>

    <div class="ui simple dropdown last">
      <i class="large icons">
        <i class="alarm outline icon"></i>
        <?php
        $notificacao = verifica_notificacao($_SESSION['usuarioId']);

        if (isset($notificacao)): ?>

        <i class="top right corner inverted green comments icon"></i>

      <?php endif ?>
    </i>
    <?= get_user_name(); ?>


    <i class="dropdown icon"></i>
    <div class="menu">
      <a class="item" href="../perfil/index_perfil.php">
      Perfil</a>
      <div class="divider"></div>
      <a href="../perfil/notificacoes.php" class="item">
        Notificações
        <?php if (isset($notificacao)): ?>

          <div class="ui label"><?php echo $notificacao; ?></div>

        <?php endif ?>
      </a>

      <a class="item" href="../controladores/sair.php">Sair</a>
    </div>
  </div>
  <?php } ?>
  <?php if (!is_user_logged_in()) { ?>
  <a href="../estatico/cad_usuario.php" class="ui inverted button">Registre-se</a>
  <?php } ?>
</div>
</div>

<div class="ui vertical inverted sidebar menu">
            <a class="item" href="../estatico/html.php">HTML</a>
            <a class="item" href="../estatico/css.php">CSS</a>
            <a class="item" href="../estatico/php.php">PHP</a>
             <?php 
  if (isset($_SESSION['tipo_user'])) {
    if ($_SESSION['tipo_user'] == 2) { ?>

    <a href="../adm/visu_sugestoes.php" class="item responsivo">ADM</a>
    <?php } 
  }
  ?>
</div>
