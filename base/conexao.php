<?php

function obterConexao()
{
    $usuario   = "root";
    $senha     = "";
    $nome_banco= "prfc";

    try 
    {
        $conexao = new PDO("mysql:host=localhost;dbname=$nome_banco;charset=utf8", $usuario, $senha);
        return $conexao;

    } catch(PDOException $erro){
        echo "Conexao falhou: ". $erro->getMessage();
    }
}
