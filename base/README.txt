--ROTEIRO PARA A INSTALAÇÃO DO SITE "PROGRAMAÇÃO IFC" RAPHAEL E JELIEL 3INFO2--

-------------------------------------------------------------------------------

1. FAZER UMA CONEXÃO SFTP EX: sftp -P 1022 raphael_kammer@191.52.62.236;

2. TRANSFERIR O ARQUIVO ZIPADO PARA O SERVIDOR EX: put programacao_ifc.zip

3. SAIR DA CONEXÃO SFTP EX: exit;

4. FAZER UMA CONEXÃO SSH EX: ssh raphael_kammer@191.52.62.236 -p 1022;

5. NAVEGAR ATÉ A PASTA QUE ESTA O ARQUIVO ZIP EX: cd public_html;

6. DESCOMPACTAR O ARQUIVO ZIP EX: gunzip programacao_ifc.zip;

7. DAR PERMISSÃO PARA GRAVAR ARQUIVOS A PASTA tcc/img/perfil EX: chmod 660 ~/public_html/tcc/img/perfil;

8. REALIZAR UMA CONEXÃO MYSQL EX: mysql -u raphael_kammer -p;

9. USAR O BANCO DISPONIVEL EX: use raphael_kammer;

10. IMPORTAR O BANCO EX: source /public_html/tcc/prfc.sql;

11. ALTERAR O ARQUIVO "conexao.php" PARA A CONEXÃO COM O BANCO EX: nano public_html/tcc/base/conexao.php (MUDANDO AS INFORMAÇÕES "$usuario" PARA O USUARIO ATUAL, "$senha" PARA A SENHA USADA NESSE USUARIO E "$nome_banco" PARA O BANCO USADO); 