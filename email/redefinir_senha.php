<?php 
include_once '../base/cabecalho.php';
include_once 'redefinidor_senha.php';

$token = $_GET['token'];
if (isset($_POST['Enviar'])) {
	$redefinidor_senha = redefinidor_senha($_POST['senha'], $_POST['senha2'], $_POST['id_user']);


}


if (isset($token)) {

	$conexao = obterConexao();
	$consulta = $conexao->query("SELECT senha, id_user FROM usuario WHERE token = '$token';");
	$dados =  $consulta->fetch(PDO::FETCH_ASSOC);


	if ($dados == false) { ?>

	<div class="ui main text container">

		<div class="ui red floating message">
			<div class="header">
				Houve um erro com sua solicitação:
			</div>
			<ul class="list">
				<li>Token Inválido!</li>
			</ul>
		</div>
		<a href="../estatico/login.php">
			<div class="ui vertical labeled icon buttons">
				<button  class="ui inverted green button">
					<i class="arrow circle outline left icon"></i>
					Voltar
				</button>
			</div>
		</a>

	</div>

	<?php }else{?>
	<div class="ui main text container">
		<div class="ui middle aligned center aligned grid">
			<div class="column">

				<?php if (isset($redefinidor_senha) && is_array($redefinidor_senha)) { ?>

				<div class="ui red floating message">
					<div class="header">
						Alguns erros foram encontrados na sua REDEFINIÇÃO:
					</div>
					<ul class="list">

						<?php


						foreach ($redefinidor_senha as $msg) {
							if ($msg != 1) {
								?> <li><?php echo($msg); ?></li>
								<?php            
							}
						}


						?>         </ul>
					</div>
					<?php }else{ ?>

					<div class="ui floating message">
						<div class="header">
							Regras:
						</div>
						<ul class="list">
							<li>Senha: 8 ou mais caracteres. (Tendo no mínimo uma letra e um número)</li>
						</ul>
					</div>

					<?php } ?>
					<div class="ui stacked segment">
						<form class="ui form" method="POST">
							<h2 class="ui dividing header">Redefina sua senha</h2>
							<div class="field">
								<div class="ui left icon input">
									<i class="lock icon"></i>
									<input type="text" name="senha" placeholder="Insira sua nova Senha">
								</div>				
							</div>
							<div class="field">
								<div class="ui left icon input">
									<i class="lock icon"></i>
									<input type="text" name="senha2" placeholder="Repita sua Senha">
								</div>				
							</div>
							<input type="hidden" name="id_user" value="<?php echo($dados['id_user']); ?>">
							<input class="ui green inverted button" name="Enviar" type="submit">
						</form>
					</div>
				</div>
			</div>
		</div>



		<?php }
	}?>
	<?php include_once '../base/rodape.php'; ?>