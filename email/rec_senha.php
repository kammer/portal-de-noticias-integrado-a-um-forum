<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once 'PHPMailer/src/PHPMailer.php';
require_once 'PHPMailer/src/Exception.php';
require_once 'PHPMailer/src/SMTP.php';
require_once "../base/conexao.php";
require '../vendor/autoload.php';

function recuperador_senha($email){

    $conexao = obterConexao();
    $consulta = $conexao->query("SELECT senha, id_user, nome FROM usuario WHERE email = '$email';");
    $dados =  $consulta->fetch(PDO::FETCH_ASSOC);

    if ($dados == false) {
        return 0;        
    }else{

        $recuperador = sha1($dados['senha'].$dados['id_user']);
        $conexao->query("UPDATE usuario SET Token = '$recuperador' WHERE email = '$email';");

    $mail = new PHPMailer(true);                            
    try {

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        //Server settings
        $mail->CharSet = 'UTF-8';
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = gethostbyname('smtp.gmail.com');  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'noreply.programacaoifc@gmail.com';      // SMTP username
        $mail->Password = 'jeliellindao';                     // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($email);
        $mail->addAddress($email);     // Add a recipient    
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');
           

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject =  'Redefina a Senha da sua Conta, Sr(a): '.$dados['nome'].' - Equipe Programação IFC';
        $mail->Body    = "Foi solicitada uma Redefinição para sua senha, se deseja proseguir, utilize o link a seguir: <a href = 'localhost/tcc/email/redefinir_senha.php?token=".$recuperador."'>Clique aqui</a>" ;
        //$mail->AltBody = 'Esta funcionando.';

        $mail->send();
        

    } catch (Exception $e) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    }
    return 1;
}
}