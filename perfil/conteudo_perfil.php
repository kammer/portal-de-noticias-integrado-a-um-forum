<div class="ui container">
  <div class="ui segments">
    <div class="ui segment">
    	<h3 class="ui header">{titulo1}</h3>
    	<p>{texto1}</p>
    </div>
    <div class="ui segment">
    	<h3 class="ui header">{titulo2}</h3>
    	<p>{texto2}</p>
    </div>
    <div class="ui segment">
    	<h3 class="ui header">{titulo3}</h3>
    	<p>{texto3}</p>
    </div>
  </div>
</div>
