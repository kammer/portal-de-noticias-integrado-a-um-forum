<?php include_once "controlador/seleciona_imagem.php"; ?>

<div class="ui vertical menu">
  <div class="item">
    <div class="header">
      <div class="ui circular image">
        <div class="ui dimmer">
          <div class="content">
            <div class="center">
              <a href="mudar_foto.php" class="ui small green inverted button">Alterar Foto</a>
            </div>
          </div>
        </div>
        <?php 
        $img = seleciona_img($_SESSION['usuarioId']);

        if (isset($img[0])) {

          ?>
          <img class="ui image" src="<?php echo($img[0]['caminho_midia']); ?>">

          <?php }else{ ?>

          <img class="ui circular image" src="../img/perfil.jpg">

          <?php } ?>
        </div>
      </div>
      <div class="menu">
        <div class="item">
          <?php
          $dados_usuario = dados_usuario();
          echo "<h2>Olá,  ".$dados_usuario[0]['nome']."!</h2>";
          ?> 
        </div>
      </div>
    </div>
    <div class="item">
      <div class="header">Opções</div>
      <div class="menu">
        <a href="notificacoes.php" class="item">
          Notificacoes
          <?php if (isset($notificacao)): ?>

            <div class="ui label"><?php echo $notificacao; ?></div>

          <?php endif ?>

        </a>
        <a href="../index.php" class="item">
          Página Principal
        </a>
        <a href="altera_dados.php" class="item">
          Editar Dados Cadastrais
        </a>
        <a href="index_perfil.php" class="item">
          Perfil
        </a>
      </div>
    </div>
  </div>

