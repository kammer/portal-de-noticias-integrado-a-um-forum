<?php 
session_start();
include_once "../controladores/controle_usuario.php";
include_once "../controladores/controle_conteudo.php";
include_once "../base/conexao.php";
require_once 'controlador/perfil_usu.php';
$dados_usuario = dados_usuario();
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <title>Perfil: <?php echo $dados_usuario[0]['nome'];?></title>
  <style type="text/css">
  body {
    background-color: #FFFFFF;
  }
  .ui.menu .item img.logo {
    margin-right: 1.5em;
  }
  .main.container {
    margin-top: 7em;
  }
  .wireframe {
    margin-top: 2em;
  }
  .ui.footer.segment {
    margin: 5em 0em 0em;
    padding: 5em 0em;
  }
</style>
<?php include_once "../base/estatico.php" ?>

</head>
<body>
  <?php include_once 'menu_perfil.php'; ?>

  <div class="fourteen wide column">
    <div class="ui main text container">

