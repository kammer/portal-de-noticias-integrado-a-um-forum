<?php 
include_once "seleciona_imagem.php";

function valida_imagem($img){

	if (!empty($img["name"])) {

		$imagem = seleciona_img($_SESSION['usuarioId']);

		if(preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $img["type"])){

			if (isset($imagem[0])) {
				$conexao = obterConexao();

				unlink($imagem[0]['caminho_midia']);

				$conexao->exec("DELETE FROM midia WHERE id_midia = ".$imagem[0]['id_midia']);

			}
		}

		$largura = 800;

		$altura = 800;

		$tamanho = 10485760;

		if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $img["type"])){
			$erros['img'] = "Isso não é uma imagem.";
		} 

		$dimensao = getimagesize($img["tmp_name"]);

		if($dimensao[0] > $largura) {
			$erros['img_largura'] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
		}

		if($dimensao[1] > $altura) {
			$erros['img_altura'] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
		}

		if($img["size"] > $tamanho) {
			$erros['img_tamanho'] = "A imagem deve ter no máximo ".$tamanho." bytes";
		}
	}else{
		$erros['img_vazio'] = "Você não selecionou uma imagem!";
	}
	if (isset($erros)) {
		return $erros;
	}else{
		return $img;
	}

}






?>