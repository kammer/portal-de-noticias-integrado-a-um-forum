<?php 

include_once "controlador/valida_imagem.php";

function cad_img ($img , $id_usuario){

	$imagem = valida_imagem($img);

	if ($imagem == $img) {

		$conexao = obterConexao();

		preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $imagem["name"], $extensao);

		$nome_imagem = md5(uniqid(time())) . "." . $extensao[1];

		$caminho_imagem = "../img/perfil/" . $nome_imagem;

		move_uploaded_file($imagem["tmp_name"], $caminho_imagem);

		$conexao->exec("INSERT INTO midia (caminho_midia, id_usuario_midia) 
			VALUES ('$caminho_imagem', $id_usuario)");

		header("Location:index_perfil.php");

	}else{
		return $imagem;
	}
}