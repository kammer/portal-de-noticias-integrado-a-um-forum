<?php 
require_once 'controlador/altera_nome.php';
require_once 'controlador/altera_apelido.php';
require_once 'controlador/altera_senha.php';
include_once "../base/cabecalho.php"; 
include_once "controlador/cad_img.php";

if (isset($_POST['enviar_nome'])) {
  $retono_nome = altera_nome($_POST['nome']);
  if ($retono_nome == "Nome alterado com sucesso :)") {
    $cor = "color: green;";
  }else{
    $cor = "color: red;";
  }
}

if (isset($_POST['enviar_apelido'])) {
  $retono_apelido = altera_apelido($_POST['apelido']);

  if ($retono_apelido == "Apelido alterado com sucesso :)") {
    $cor = "color: green;";
  }else{
    $cor = "color: red;";
  }
}

if (isset($_POST['enviar_senha'])) {
  $retono_senha = altera_senha($_POST['senha'], $_POST['nova_senha'], $_POST['nova_senha2']);

  if ($retono_senha == "Senha alterada com sucesso :)") {
    $cor = "color: green;";
  }else{
    $cor = "color: red;";
  }
}

?>

<?php require_once 'controlador/perfil_usu.php'; $dados_usuario = dados_usuario(); ?>
<div class="ui main container">
  <div class="ui container adm">
    <div class="ui two column stackable grid">
      <div class="column painel">
        <?php include_once "menu_perfil.php" ?>
      </div>
      <div class="column conteudo">

        <div class="ui styled accordion">
          <div class="title">
            <i class="dropdown icon"></i>
            Alterar Nome
          </div>
          <div class="content">
            <form class="ui form" method="POST">
              <div class="fields">  
                <div class="eight wide field">
                  <input type="text" name="nome" placeholder="Insira seu novo Nome">
                </div>
              </div>
              <input class="ui inverted green button" type="submit" name="enviar_nome" value="Enviar">
            </form>
          </div>
          <div class="title">
            <i class="dropdown icon"></i>
            Alterar Apelido
          </div>
          <div class="content">
            <form class="ui form" method="POST">
              <div class="fields">  
                <div class="eight wide field">
                  <input type="text" name="apelido" placeholder="Insira seu novo Apelido">
                </div>
              </div>
              <input class="ui inverted green button" type="submit" name="enviar_apelido" value="Enviar">
            </form>
          </div>
          <div class="title">
            <i class="dropdown icon"></i>
            Alterar Senha
          </div>
          <div class="content">
            <form class="ui form" method="POST">

              <div class="fields">  
                <div class="eight wide field">
                  <label>Senha Atual</label>

                  <input type="password" name="senha" placeholder="**********">

                </div>
              </div>
              <div class="fields">  
                <div class="eight wide field">
                  <label>Nova Senha</label>
                  <input type="password" name="nova_senha" placeholder="**********">

                </div>
              </div>
              <div class="fields">
                <div class="eight wide field">
                  <label>Confirme a Nova Senha</label>
                  <input type="password" name="nova_senha2" placeholder="**********">
                </div>

              </div>
              <input class="ui inverted green button" type="submit" name="enviar_senha" value="Enviar"> 
            </form>
          </div>
          <div class="title">
            <i class="dropdown icon"></i>
            Alterar Ocupação
          </div>
          <div class="content">
            <form class="ui form" method="POST">
              <div class="fields">
                <div class="six wide field">
                  <label>Ocupação</label>
                  <select class="ui fluid dropdown" name="profissao">
                    <option  value="">Selecione uma Opção</option>
                    <option value="Profissional da Área">Profissional da Área</option>
                    <option value="Professor">Professor</option>
                    <option  value="Aluno">Aluno</option>
                  </select>
                </div> 

              </div>
              <input class="ui inverted green button" type="submit" name="enviar_profissao" value="Enviar">  
            </form>
          </div>


        </div>
      </div>
    </div>
  </div>
</div>
<?php include_once "rodape.php" ?>

