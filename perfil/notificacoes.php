<?php include_once "../base/cabecalho.php";  ?>
<?php require_once 'controlador/perfil_usu.php'; $dados_usuario = dados_usuario(); ?>
<?php include_once "controlador/exibe_notificacoes.php"; ?>
<?php include_once "controlador/seleciona_notificacoes.php"; ?>
<?php include_once "controlador/seleciona_usuario_notificador.php"; ?>

<div class="ui main container">
  <div class="ui container adm">
    <div class="ui two column stackable grid">
      <div class="column painel">
        <?php include_once "menu_perfil.php" ?>
      </div>
      <div class="column conteudo">
        <div class="ui two stackable cards">
          <?php

          $exibe_notificacoes = exibe_notificacoes($_SESSION['usuarioId'], 1);
          $nr_notificacoes = count($exibe_notificacoes);

          $i=0;
          $a=0;

          while ($a < $nr_notificacoes) { 

            $notificacoes_comentario = seleciona_notificacoes_comentario($exibe_notificacoes[$a]['id_usuario_notificador'],$_SESSION['usuarioId']);
            $nr_not = count($notificacoes_comentario);

            if ($nr_not > 1) {
              for ($o=0; $o < $nr_not ; $o++) { 

                ?>

                <div class="card">
                  <div class="content">
                    <img class="right floated mini ui image" src="/images/avatar/large/elliot.jpg">
                    <div class="header">
                      Notificação de comentario:
                      <?php echo$notificacoes_comentario[$o]['nome']; ?>
                    </div>
                    <div class="meta">
                      Titulo Post: <?php echo$notificacoes_comentario[$o]['titulo_post'] ?> Data: <?php echo$notificacoes_comentario[$o]['data_comentario'];?>
                    </div>
                    <div class="description">
                      <?php echo$notificacoes_comentario[$o]['texto_comentario'];?>
                    </div>
                  </div>
                  <div class="extra content">
                    <a href="controlador/deletar_notificacao.php?id_post=<?php echo$notificacoes_comentario[$o]['id_post']; ?>&id=<?php echo($exibe_notificacoes[$a]['id_notificacao']); ?>" class="ui bottom green basic attached button">
                      Ir para a notificação  
                    </a>
                  </div>
                </div>

                <?php
                $a++;
              }
            }else{ ?>

            <div class="card">
              <div class="content">
                <img class="right floated mini ui image" src="/images/avatar/large/elliot.jpg">
                <div class="header">
                  Notificação de comentario:
                  <?php echo$notificacoes_comentario[$i]['nome']; ?>
                </div>
                <div class="meta">
                  Titulo Post: <?php echo$notificacoes_comentario[$i]['titulo_post'] ?> Data: <?php echo$notificacoes_comentario[$i]['data_comentario'];?>
                </div>
                <div class="description">
                  <?php echo$notificacoes_comentario[$i]['texto_comentario'];?>
                </div>
              </div>
              <div class="extra content">
                <a href="controlador/deletar_notificacao.php?id_post=<?php echo$notificacoes_comentario[$i]['id_post']; ?>&id=<?php echo($exibe_notificacoes[$a]['id_notificacao']); ?>" class="ui bottom green basic attached button">
                  Ir para a notificação  
                </a>
              </div>
            </div>

            <?php
            $a++;
          }
        }

        $exibe_notificacoes = exibe_notificacoes($_SESSION['usuarioId'], 2);
        $nr_notificacoes = count($exibe_notificacoes);

        $i=0;
        $a=0;

        while ($a < $nr_notificacoes) { 

          $notificacoes_resposta = seleciona_notificacoes_resposta($_SESSION['usuarioId'],$exibe_notificacoes[$a]['id_usuario_notificador']);
          $nr_not = count($notificacoes_resposta);

          if ($nr_not > 1) {
            for ($o=0; $o < $nr_not ; $o++) { 

              ?>

              <div class="card">
                <div class="content">
                  <img class="right floated mini ui image" src="/images/avatar/large/elliot.jpg">
                  <div class="header">
                    Notificação de resposta:
                    <?php echo$notificacoes_resposta[$o]['nome']; ?>
                  </div>
                  <div class="meta">
                    Titulo Post: <?php echo$notificacoes_resposta[$o]['titulo_post'] ?> Data: <?php echo$notificacoes_resposta[$o]['data_resposta'];?> 
                  </div>
                  <div class="description">
                    <?php echo$notificacoes_resposta[$o]['texto_resposta'];?>
                  </div>
                </div>
                <div class="extra content">
                  <a href="controlador/deletar_notificacao.php?id_post=<?php echo$notificacoes_resposta[$o]['id_post']; ?>&id=<?php echo($exibe_notificacoes[$a]['id_notificacao']); ?>" class="ui bottom green basic attached button">
                    Ir para a notificação  
                  </a>
                </div>
              </div>

              <?php
              $a++;
            }
          }else{ ?>

          <div class="card">
            <div class="content">
              <img class="right floated mini ui image" src="/images/avatar/large/elliot.jpg">
              <div class="header">
                Notificação de resposta:
                <?php echo$notificacoes_resposta[$i]['nome']; ?>
              </div>
              <div class="meta">
                Titulo Post: <?php echo$notificacoes_resposta[$i]['titulo_post'] ?> Data: <?php echo$notificacoes_resposta[$i]['data_resposta'];?>
              </div>
              <div class="description">
                <?php echo$notificacoes_resposta[$i]['texto_resposta'];?>
              </div>
            </div>
            <div class="extra content">
              <a href="controlador/deletar_notificacao.php?id_post=<?php echo$notificacoes_resposta[$i]['id_post']; ?>&id=<?php echo($exibe_notificacoes[$a]['id_notificacao']); ?>" class="ui bottom green basic attached button">
                Ir para a notificação  
              </a>
            </div>
          </div>

          <?php
          $a++;
        }
      }

      ?>


    </div>
  </div>
</div>
</div>
</div>

<?php include_once "../base/rodape.php"; ?>