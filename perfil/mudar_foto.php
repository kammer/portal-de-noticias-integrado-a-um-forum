<?php include_once "../base/cabecalho.php"; ?>
<?php 
include_once "controlador/cad_img.php";

if (isset($_POST['enviar'])) {
  $retorno_img = cad_img($_FILES['foto_perfil'], $_SESSION['usuarioId']);
} 
?>
<div class="ui main container">
  <h2 class="ui dividing header">Alteração de Foto:</h2>
</div>
<div class="ui container adm">
  <div class="ui column stackable grid">
    <div class="column">
      <?php if (isset($retorno_img) && is_array($retorno_img)) { ?>

      <div class="ui red floating message">
        <i class="close icon"></i>
        <div class="header">
          Alguns erros foram encontrados em sua IMAGEM:
        </div>
        <ul class="list">
          <?php foreach ($retorno_img as $erros_imagem) { ?>
          <li><?php echo $erros_imagem ?></li>
          <?php } ?>
        </ul>
      </div>

      <?php }else{ ?>

      <div class="ui green floating message">
        <div class="header">
          Regras para a Imagem:
        </div>
        <ul class="list">
          <li>Largura e Altura: 800x800;</li>
          <li>Extenções Permitidas: pjpeg | jpeg | png | gif | bmp</li>
        </ul>
      </div>

      <?php } ?>
      <form class="ui form" method="POST" enctype="multipart/form-data">
        <div class="field">
          <img class="ui medium circular main image imagem" id="preview" src="">
          <label for="mudar-foto" class="ui orange fluid button">Selecione Sua Foto</label>
          <input type="file" id="mudar-foto" name="foto_perfil" placeholder="Insira o titulo da TAG">
        </div>
        <input class="ui inverted right floated green button" type="submit" name="enviar" value="Proximo">
      </div>
    </form>
  </div>
</div>
</div>



<?php include_once "../base/rodape.php";?>