<?php 
require_once '../controladores/select_user.php';
require_once 'upar_usuario.php';
$lista_user = select_user();
?>
<?php require_once '../base/cabecalho.php'; ?>

<div class="ui main container">
	<h2 class="ui dividing header">Painel Administrativo</h2>
</div>

<div class="ui container adm">
	<div class="ui two column stackable grid">
		<!-- <div class="four wide column"> -->

			<?php include_once "painel_adm.php" ?>

			<!-- </div> -->
			<div class="column conteudo">
				<div class="ui divided items">

					<?php 
					foreach ($lista_user as $user) {
						if ($user['id_user'] != $_SESSION['usuarioId']) {
							?>

							<div class="item">
								<div class="content">
									<div class="header"><?php echo $user['nome']; ?></div>
									<div class="meta">

										<a>Apelido: <?php echo $user['apelido']; ?></a>
										<a>Tipo: <?php 

										if ($user['id_tipo'] == 2) {

											echo "ADMINISTRADOR";

										}else{

											echo "USUARIO";

										} 

										?></a>

										<a>Email: <?php echo $user['email']; ?></a>

									</div>
									<div class="description">

										<p>Ocupação : <?php echo $user['profissao']; ?></p>

									</div>
									<div class="extra">
										<a href="banir.php?id=<?php echo$user['id_user'];?>&situacao=<?php echo$user['situacao'];?>">

											<?php if ($user['situacao'] == TRUE): ?>

												<div class="ui right floated green inverted button">
													Desbanir 
													<i class="right chevron icon"></i>
												</div>

											<?php else: ?>

												<div class="ui right floated red inverted button">
													Banir
													<i class="right chevron icon"></i>
												</div>

											<?php endif ?>

										</a>
										<a href="upar.php?id=<?php echo$user['id_user'];?>&tipo=<?php echo$user['id_tipo'];?>">

											<?php if ($user['id_tipo'] == 2): ?>

												<div class="ui right floated red inverted button">
													Rebaixar
													<i class="right chevron icon"></i>
												</div>

											<?php else: ?>

												<div class="ui right floated green inverted button">
													Promover
													<i class="right chevron icon"></i>
												</div>

											<?php endif ?>
											
										</a>	
										Situação:
										<div class="ui label"><?php if ($user['situacao'] == TRUE){echo "Banido";}else{echo "Normal";} ?></div>
									</div>
								</div>
							</div>

							<?php	}
						}
						?>
					</div>
				</div>
			</div>
		</div>


		<?php require_once '../base/rodape.php'; ?>


