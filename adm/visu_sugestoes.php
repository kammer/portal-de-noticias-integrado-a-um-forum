<?php 
require_once '../base/cabecalho.php';
require_once '../controladores/select_sugestao.php';
$sugestoes = select_sugestao();
?>

<div class="ui main container">
	<h2 class="ui dividing header">Painel Administrativo</h2>
</div>
<div class="ui container adm">
	<div class="ui two column stackable grid">
		<!-- <div class="column"> -->
			<?php include_once "painel_adm.php" ?>
			<!-- </div> -->
			<div class="column conteudo">
				<h2 class="centralizar">SUGESTÕES</h2>
				<?php 
				foreach ($sugestoes as $exibe_sugestoes) {

					?>
					<div class="ui segment">
						<div class="ui items">
							<div class="item">

								<div class="content">
									<div class="ui center header"> <?php echo ($exibe_sugestoes['titulo_sug']); ?>

									</div>
									<a href="../controladores/deleta_sugestao.php?id=<?php echo $exibe_sugestoes['id_sug']; ?>">
										<i class="trash icon"></i>	
									</a>

									<p class="description"><?php echo ($exibe_sugestoes['texto_sug']); ?></p>
									<div class="extra">
										<div><?php echo ($exibe_sugestoes['nome']); ?>

										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>

		<?php require_once '../base/rodape.php'; ?>