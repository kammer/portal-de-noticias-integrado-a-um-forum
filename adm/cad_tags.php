<?php 
require_once '../base/cabecalho.php'; 
require_once '../controladores/trata_tag.php';

if (isset($_POST['enviar'])) {
	$retorno_tag = trata_tag($_POST['titulo'] , $_POST['texto_tag'] , $_POST['exemplo'], $_POST['linguagem']);

	if ($retorno_tag == "Cadastro efetuado com sucesso") {
		$cor = "color: green;";
	}else{
		$cor = "color: red;";
	}
}
?>
<div class="ui main container">
	<h2 class="ui dividing header">Painel Administrativo</h2>
</div>
<div class="ui container adm">
	<div class="ui two column stackable grid">
		<!-- <div class="four wide column"> -->
			<?php include_once "painel_adm.php"; ?>
			<!-- </div> -->
			<div class="column conteudo">
				<form class="ui form" method="POST">
					<h2 class="ui dividing header">Cadastro de Tags</h2>

					<?php if (!isset($retorno_tag['msg'])) {?>

					<div class="ui floating message">
						<div class="header">
							Regras:
						</div>
						<ul class="list">
							<li>O TITULO tem que ter no minimo 5 e no maximo 80 caracteres.</li>
							<li>O TEXTO tem que ter no minimo 25 e no maximo 2000 caracteres.</li>
							<li>O EXEMPLO tem que ter no minimo 25 e no maximo 256 caracteres.</li>
						</ul>
					</div>

					<?php }elseif (is_array($retorno_tag['msg'])) { ?>

					<div class="ui red floating message">
						<div class="header">
							Alguns erros foram encontrados no seu CADASTRO:
						</div>
						<ul class="list">
							<?php

							$mensagem = $retorno_tag['msg'];

							foreach ($mensagem as $msg_array) {
								if ($msg_array != 1) {
									foreach ($msg_array as $msg) {
										?> <li><?php echo($msg); ?></li>
										<?php            
									}
								}
							}
							?>         </ul>
						</div>
						<?php 


					}?>

					<div class="field">
						<label>Titulo Tag</label>
						<input type="text" name="titulo" placeholder="Insira o titulo da TAG">
					</div>
					<div class="field">
						<label>Código de exemplo</label>
						<input type="text" name="exemplo" placeholder="Codigo de exemplo">
					</div>
					<div class="field">
						<div class="field">
							<label>Texto sobre a tag</label>
							<textarea name="texto_tag" id="" cols="84" rows="5"></textarea>
						</div>
					</div>
					<div class="field">
						<div class="eight wide field">
							<label>Selecionar linguagem que a tag pertence</label>
							<select class="ui fluid dropdown" name="linguagem">
								<option value="">Selecione uma Opção</option>
								<option value="1">PHP</option>
								<option value="2">HTML</option>
								<option value="3">CSS</option>
							</select>
						</div>
					</div>
					<input class="ui inverted right floated green button" type="submit" name="enviar" value="Proximo">
				</form>
			</div>
		</div>
	</div>
</div>

<?php require_once '../base/rodape.php'; ?>