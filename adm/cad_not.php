<?php
require_once '../base/cabecalho.php';
require_once '../controladores/trata_not.php';
require_once '../forum/controlador/data_atual.php';
require '../controladores/cadastro_noticia.php';

$data = dataAtual();

if (isset($_POST['enviar'])) {
    $retorno_not =  trata_not($_POST['titulo_not'] , $_POST['texto_not'] , $data);

    if ($retorno_not['msg'] == "Noticia Cadastrada com sucesso") {

    }else{
        $titulo = $retorno_not['titulo'];
        $texto = $retorno_not['texto'];
    }

}


?>
<div class="ui main container">
    <h2 class="ui dividing header">Painel Administrativo</h2>
</div>
<!-- <div class="ui container"> -->
    <div class="ui container adm">
        <div class="ui two column stackable grid">
            <!-- <div class="four wide column"> -->
                <?php include_once "painel_adm.php" ?>
                <!-- </div> -->
                <div class="column conteudo">
                    <form class="ui form" method="POST">
                        <h2 class="ui dividing header">Cadastro</h2>

                        <?php if (!isset($retorno_not['msg'])){ ?>

                        <div class="ui floating message">
                            <div class="header">
                                Regras:
                            </div>
                            <ul class="list">
                                <li>O TITULO tem que ter no minimo 5 e no maximo 240 caracteres.</li>
                                <li>O TEXTO tem que ter no minimo 25 e no maximo 5000 caracteres.</li>
                            </ul>
                        </div>

                        <?php }elseif (is_array($retorno_not['msg'])) { ?>

                        <div class="ui red floating message">
                            <div class="header">
                                Alguns erros foram encontrados na sua NOTICIA:
                            </div>
                            <ul class="list">
                                <?php

                                $mensagem = $retorno_not['msg'];

                                foreach ($mensagem as $msg_array) {
                                    if ($msg_array != 1) {
                                        foreach ($msg_array as $msg) {
                                            ?> <li><?php echo($msg); ?></li>
                                            <?php            
                                        }
                                    }
                                }
                                ?>         
                            </ul>
                        </div>
                            <?php 


                        }?>

                        <div class="fields">    
                            <div class="field">
                                <label>Titulo Notícia</label>
                                <input type="text" name="titulo_not" placeholder="" data-inverted="" data-tooltip="Só permitidas letras e espaços!" data-position="bottom left">

                                <label>Texto texto notícia</label>
                                <textarea name="texto_not" id="" cols="84" rows="5"></textarea>
                                <input class="ui inverted right floated green button" type="submit" name="enviar">
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- </div> -->
    </div>
</div>

<?php require_once '../base/rodape.php'; ?>